
""" This bot has no purpose other than to be a warm, cuddly kitten
    in its channels.

    It will:
      reply "<3" whenever given a botsnack
      purr [and potentially curl up with the user] when scritched
      reply "=^.^=" or similar when otherwise directly addressed
      mew [or say "mew?" when mentioned by name]. """

import re
import random
from threading import Timer
from irclib import nm_to_n
from commandbot import CommandBot
import config
from cslounge import csloungify
from modules.command import CharTriggerCommand

version = "kittenbot v0.1 by bswolf"

botsnack = re.compile(r"botsnack[.!]?")
scritch = re.compile(r"(?:pets?|hugs?|scritch(?:es)?|cuddles?(?: with)?) (.*)")
tscritch = re.compile(r"(scritch|\*scritch\*|hug|\*hug\*)")

class KittenBot(CommandBot):
    def __init__(self, *args, **kwargs):
        super(KittenBot, self).__init__(*args, **kwargs)
        self.botsnack = { "reply" : ["<3", "<3", "^_^"],
                          "act" : ["happily noms on the botsnack. ^_^",
                                   ("noms on the botsnack and "
                                    "looks at $src expectantly."),
                                   "nom. ^_^"],
                        }
        self.scritch = { "act" : ["purrs.", "purrs.", "purrs.", "purrs.",
                                  "purrs and curls up next to $src.",
                                  "purrs and curls up on $src's lap."],
                       }
        self.what = { "reply" : ["=^.^=", "=^.^=", "...mew?"],
                      "act" : ["stares at $src.",
                               "stares innocently at $src."],
                    }
        self.mention = { "reply" : ["...mew?"] }
        config.add_config(self, ["botsnack", "scritch", "mention", "what"])
        self.commandlist.add_admin_command(
                CharTriggerCommand(self.cmdchar, "add_reaction",
                                   [str, str, str], self.add_reaction))
        self.commandlist.add_admin_command(
                CharTriggerCommand(self.cmdchar, "rem_reaction",
                                   [str, str, str], self.rem_reaction))

    @staticmethod
    def _select_response(d, src):
        f = []
        if "reply" in d:
            f += [ ("reply", s) for s in d["reply"] ]
        if "act" in d:
            f += [ ("act", s.replace("$src", src)) for s in d["act"] ]
        if f:
            return random.choice(f)

    def add_reaction(self, src, chat, ev, t, text):
        """ add_reaction <event> <type> <text>: Add a reaction. event is one
            of 'botsnack', 'mention', 'scritch', 'what'. type is one of 'reply'
            and 'act'. text should end with punctuation or a smiley. """
        if ev not in ['botsnack', 'scritch', 'mention', 'what']:
            return "I can't add reactions for '%s'. =-.-=" % ev
        if t not in ['reply', 'act']:
            return "I can't add reactions of the type '%s'. =-.-=" % t
        d = getattr(self, ev)
        if t not in d:
            d[t] = [text]
        else:
            d[t] += [text]
        return "Okay. :3"

    def rem_reaction(self, src, chat, ev, t, text):
        """ rem_reaction <event> <type> <text>: Remove a reaction. event is one
            of 'botsnack', 'mention', 'scritch', 'what'. type is one of 'reply'
            and 'act'. text must be an exact match. """
        if ev not in ['botsnack', 'scritch', 'mention', 'what']:
            return "I don't have reactions for '%s'. =-.-=" % ev
        d = getattr(self, ev)
        if t not in d:
            return "I don't have %s reactions of the type '%s'. =-.-=" % t
        if text in d[t]:
            d[t].remove(text)
            return "Okay. :3"
        else:
            return "I didn't have that to begin with. =-.-="

    def scritched_me(self, c, action):
        m = scritch.match(action)
        if m:
            t = m.group(1)
            # in case it's a list
            v = t.split(',')
            v[-1:] = v[-1].split(' and ')
            if len(v) == 1:
                if t.startswith(c.get_nickname()):
                    return True
            for n in v:
                n = n.strip()
                if n.startswith(c.get_nickname()):
                    return True
        return False

    def mention_me(self, c, msg):
        n = c.get_nickname().lower()
        m = msg.lower()
        triggers = [n, "kitten", "kitty"] # kitten counts for "kittens"
        return any((t in m for t in triggers))

    def on_action(self, c, e):
        src = e.source()
        chan = e.target()
        msg = e.arguments()[0]
        if len(msg) < 1:
            return
        if src:
            src = nm_to_n(src)
            if chan != c.get_nickname():
                if self.floodcontrol.checkignore(src, chan):
                    return None
                if self.scritched_me(c, msg):
                    result = self._select_response(self.scritch, src)
                else:
                    result = self.handle_untargeted(c, e, src, msg, True)
                if result and result != True:
                    self.floodcontrol.ensuredelay(chan)
                    if result[0] == "reply":
                        self.pubmsg(chan, result[1], target=src)
                    elif result[0] == "act":
                        self.action(chan, result[1])
                    else:
                        # no mark
                        return None
                    self.floodcontrol.markuser(src, chan, e)
            else:
                if self.scritched_me(c, msg):
                    result = self._select_response(self.scritch, src)
                # We don't use what or mention cases in private messages
                if result and result != True:
                    if result[0] == "reply":
                        self.privmsg(src, result[1])
                    elif result[0] == "act":
                        self.action(src, result[1])

    def handle_targeted(self, c, e, src, msg):
        if botsnack.match(msg):
            r = self._select_response(self.botsnack, src)
        elif tscritch.match(msg):
            r = self._select_response(self.scritch, src)
        elif "the game" in msg.lower():
            r = ("reply", "...mao?")
        else:
            if e.target() == c.get_nickname():
                # arbitrary messages only work in channel
                return None
            elif '.' in e.target():
                # server-type notices
                return None
            r = self._select_response(self.what, src)
        if r:
            if r[0] == "reply":
                return r[1]
            elif r[0] == "act":
                if e.target() != c.get_nickname():
                    self.floodcontrol.ensuredelay(e.target())
                    self.action(e.target(), r[1])
                    self.floodcontrol.markuser(src, e.target(), e)
                else:
                    self.action(e.source(), r[1])
                return True

    def handle_untargeted(self, c, e, src, msg, from_action=False):
        if self.mention_me(c, msg):
            r = None
            if c.get_nickname() in msg:
                rset = random.choice([self.what, self.mention])
                r = self._select_response(rset, src)
            elif e.target() in self.floodcontrol.freechannels:
                if random.randint(0, 2) == 0:
                    r = self._select_response(self.mention, src)
            if from_action:
                return r
            if r:
                if r[0] == "reply":
                    return r[1]
                elif r[0] == "act":
                    if e.target() != c.get_nickname():
                        self.floodcontrol.ensuredelay(e.target())
                        self.action(e.target(), r[1])
                        self.floodcontrol.markuser(src, e.target(), e)
                    else:
                        self.action(e.source(), r[1])
                    return True

    # In-channel temporary muting
    def channel_mute(self, src, chat, *args):
        """ Silence the bot in the channel for 5 minutes. """
        self.action(chat, "takes a 5-minute nap.")
        self.muted[chat] = True
        def um():
            self.muted[chat] = False
        Timer(300.0, um).start()
        return True

    # Override the default quit message.
    def quit(self, src, chat):
        """ quit: Shut the bot down. """
        self.stop("Taking a nap.")

if __name__ == "__main__":
    from pswd import kpswd
    hostname = 'irc.freenode.net'
    port = 6667
    name = "csloungekitten"
    chanlist = ['##cslounge-kittens']
    admins = ['Zannick']
    version = "kittenbot v0.1 by bswolf"
    bot = KittenBot(hostname, port, chanlist, name, admins, password=kpswd,
                    cmdchar='%', version=version, username='cuddles')
    bot = csloungify(bot)
    bot.floodcontrol.mintime = 60
    bot.floodcontrol.freechannels = ['##cslounge-kittens']
    bot.floodcontrol.responsedelay = 1
    bot.start()
