# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""A simple base class for use as an irc bot.

This class is the starting point for irc bots built in this framework. It
contains the proper functionality for the Logger and FloodControl modules, and
the Command module which is the major feature of this implementation.

Additional modules can be added to a bot instance of this class without the
need for creating subclasses, unless further integration with the bot is
necessary (as this may require overriding some CommandBot functions).
"""

import re
import sys
import time
import traceback
from functools import partial
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, IRC
from threading import Timer
import config
from modules.command import CommandList, RegexCommand, CharTriggerCommand, IGNORE
from modules.module import Module, extract_module_name
from modules.logger import Logger
from modules.floodcontrol import FloodControl

# The current version of IRC._handle_event invokes all the
# all_events handlers first, defying priority order.
def _handle_event(self, connection, event):
    """[Internal]"""
    h = self.handlers
    all_handlers = h.get("all_events", []) + h.get(event.eventtype(), [])
    for handler in sorted(all_handlers):
        if handler[1](connection, event) == "NO MORE":
            return
IRC._handle_event = _handle_event

nickserv = 'NickServ'
chanserv = 'ChanServ'
rootdir = './'
muteregex = re.compile(r"(be )?quiet", re.I)

version = "CommandBot 0.5 by bswolf"

class CommandBot(SingleServerIRCBot, object):
    """ Implements a bot that can chat on IRC.
        Its capabilities include: autoidentification to NickServ, logs
        of all chats it participates in, responses to a set of commands,
        flood control, saveable configuration, and extensibility through
        the use of modules. """
    def __init__(self, hostname, port, chanlist, name, admins,
            password=None, apass=None, cmdchar='!', pm_only_cmds=True,
            modules=[], version=version, username=None):
        """ hostname - IRC server hostname to connect to
            port - port on the server
            chanlist - list of channels to join upon connecting
            name - nick of the bot
            admins - nonempty list of nicknames of bot admins
            password - the password the bot uses to identify itself to NickServ
            apass - admin password that admins must use to identify
                    themselves to the bot. if None, admins need only to be
                    identified to NickServ when talking to the bot
            cmdchar - the command character for help, cmdinfo, and acmdinfo
            pm_only_cmds - True if help, cmdinfo, version are PM-only commands
            modules - a list of Module objects to be installed.
            version - a version string about the bot.
            username - the username field for irc (as seen via whois). """
        self.host = hostname
        self.port = port
        self.nick = name
        self.realname = "%s (bot run by %s)" % (name, admins[0])
        self.username = username or name
        if password:
            self.pswd = password
            self.haspswd = True
        else:
            self.haspswd = False
        SingleServerIRCBot.__init__(self, [(self.host, self.port)],
                                    self.nick, self.realname)

        self.chanlist = chanlist
        self.adminlist = admins
        self.adminconfirmed = [] if apass else admins
        self.apass = apass

        self.highlights = [name]
        self.capab_id = True
        for ev in ["privmsg", "pubmsg", "action", "privnotice", "pubnotice"]:
            self.connection.add_global_handler(ev, self.handle_capab_id, -20)

        self.muted = {} # Set to True per channel to disable responses

        self.version = version + ", Python " + sys.version.replace('\n', ', ')
        self.errorlog = 'important.log' # puts it in the current directory
        self.rootdir = rootdir
        self.configfile = rootdir + "%s.conf" % self.nick
        self.config = ['adminlist', 'chanlist', 'highlights', 'muted']

        self.cmdchar = cmdchar
        self.commandlist = CommandList(cmdchar, pm_only_cmds)
        # The second group will be empty if there is no space after the first
        admin = re.compile(re.escape(cmdchar) + r"admin ([^ ]+) ?(.*)")
        admin_cmds = [(RegexCommand(admin, cmdchar + "admin", self.admin)),
                CharTriggerCommand(cmdchar, "join", [str], self.c_join),
                CharTriggerCommand(cmdchar, "mute", [str], self.mute),
                CharTriggerCommand(cmdchar, "part", [str], self.c_part),
                CharTriggerCommand(cmdchar, "quit", [], self.quit),
                CharTriggerCommand(cmdchar, "unmute", [str], self.unmute)]
        priv_cmds = [RegexCommand(admin, "/admin", self.not_admin)]
        v = CharTriggerCommand(cmdchar, "version", [], self._version)
        if pm_only_cmds:
            priv_cmds += [v]
        else:
            self.commandlist.add_public_command(v)
        for acmd in admin_cmds:
            self.commandlist.add_admin_command(acmd)
        for pcmd in priv_cmds:
            self.commandlist.add_private_command(pcmd)
        m = RegexCommand(muteregex, "/channel_mute", self.channel_mute)
        self.commandlist.add_public_command(m, pm_option=IGNORE, targeted=True)

        self.modules = {self.nick: self}
        self.install_module(Logger(self.rootdir + 'logs/%s/' % self.nick,
                self.highlight, self.channels, cmdchar=cmdchar))
        self.logger = self.modules[extract_module_name(Logger)]
        self.install_module(FloodControl(self.isAdmin, self.privmsg,
                                         cmdchar=cmdchar))
        self.floodcontrol = self.modules[extract_module_name(FloodControl)]
        for module in modules:
            self.install_module(module)

    def _connect(self):
        """[Internal]"""
        password = None
        if len(self.server_list[0]) > 2:
            password = self.server_list[0][2]
        try:
            self.connect(self.server_list[0][0],
                         self.server_list[0][1],
                         self._nickname,
                         password,
                         username=self.username,
                         ircname=self._realname)
        except ServerConnectionError:
            pass

    ## Modules ##

    def install_module(self, module):
        mclass = extract_module_name(module.__class__)
        self.modules[mclass] = module
        for handler_args in module.get_handlers():
            self.connection.add_global_handler(*handler_args)
        admin, private, public = module.get_commands()
        for command in admin:
            self.commandlist.add_admin_command(command)
        for command in private:
            self.commandlist.add_private_command(command)
        for command, pm_option, targeted in public:
            self.commandlist.add_public_command(command, pm_option, targeted)
        # Don't need to get the config variables since they'll be added
        # back to the config list in the module itself.

    ## Internal IRC handling ##

    def handle_capab_id(self, c, e):
        if self.capab_id:
            msg = e._arguments[0]
            if msg.startswith('+'):
                msg = msg[1:]
                e.source_identified = True
                if len(msg) == 0:
                    self.next_is_id = True
            else:
                if msg.startswith('-'):
                    msg = msg[1:]
                    e.source_identified = False
                elif hasattr(self, "next_is_id") and self.next_is_id:
                    e.source_identified = True
                    del self.next_is_id
                else:
                    e.source_identified = False
            e._arguments[0] = msg

    def highlight(self, eventtype,  msg):
        """ Determines whether a public message highlights the bot.
            Returns true if the msg contains one of the highlight expressions,
            or is a public message that would cause a command to be evaluated.
            This is not a reliable function for private messages. """
        for highlight in self.highlights:
            if re.search(highlight, msg, re.I):
                return True
        if eventtype in ["pubmsg", "pubnotice"]:
            if self.commandlist.match_public_command(msg):
                return True
        return False

    def on_motd(self, c, e):
        print e.arguments()[0]

    def on_ping(self, c, e):
        pass

    ## Major error log ##

    def logimportant(self, msg):
        with open(self.errorlog, 'a') as logfile:
            output = "%s: %s\n" % (time.ctime(time.time()), msg)
            logfile.write(output)

    ## Joining IRC, handling NickServ ##

    def on_welcome(self, c, e):
        if self.capab_id:
            c.send_raw("CAPAB IDENTIFY-MSG")
            c.send_raw("CAP REQ :IDENTIFY-MSG")

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + '_')
        self.ghost(c)

    def ghost(self, c):
        if self.haspswd:
            c.privmsg(nickserv, "ghost " + self.nick + " " + self.pswd)

    def identify(self, c):
        if self.haspswd and self.nick == c.get_nickname():
            c.privmsg(nickserv, "identify " + self.pswd)

    def handle_nickserv(self, c, e):
        msg = e.arguments()[0]
        if self.haspswd:
            if msg.startswith('This nickname is registered.'):
                self.identify(c)
            elif msg.endswith('is not online.'):
                if self.nick != c.get_nickname():
                    c.nick(self.nick)
            elif msg.endswith('not a registered nickname.'):
                if hasattr(self, "reghost") and self.reghost:
                    self.logimportant("Can't ghost, not registered.")
                    self.haspswd = False
                else:
                    self.reghost = True
                    self.ghost(c) # Ensure that the name is not in use
            elif msg.endswith('has been ghosted.'):
                if hasattr(self, "reghost") and self.reghost:
                    self.reghost = False
                if self.nick != c.get_nickname():
                    c.nick(self.nick)
            elif msg.startswith('Invalid password'):
                # we just failed at ghosting or identifying
                self.logimportant('Bad password.')
                self.haspswd = False
                if c.get_nickname() != self.nick + '_':
                    c.nick(self.nick + '_')
            elif msg.startswith('You are now identified'):
                for chan in self.chanlist:
                    self.join(chan)
            else:
                self.logimportant('NickServ: '+msg)
        elif msg.startswith('This nickname is registered.'):
            c.nick(c.get_nickname() + '_')

    ## IRC Event Handlers ##
    # These handlers operate at -10 priority, and are called by _dispatcher.

    def on_ctcp(self, c, e):
        """ Handles ctcp version requests. """
        src = e.source()
        if src:
            src = nm_to_n(src)
        args = e.arguments()
        msgtype = args[0]
        if msgtype.lower() == 'version':
            msg = self._version()
            c.ctcp_reply(src, msgtype + ' ' + msg)
            self.logger.logself(c, 'ctcpreply', target=src,
                                msg=msg, ctcptype=msgtype)

    def on_quit(self, c, e):
        """ If admins have to supply a password to be recognized by the bot as
            an admin, then the bot has to forget that authorization when they
            quit. """
        src = e.source()
        if src:
            src = nm_to_n(src)
        if self.apass and src in self.adminconfirmed:
            self.adminconfirmed.remove(src)

    ## Helper response methods ##

    def action(self, chan, msg):
        """ Sends an action to the specified channel. """
        self.connection.action(chan, msg)
        self.logger.logself(self.connection, "action", target=chan, msg=msg)

    def join(self, chan):
        """ Joins a channel. """
        self.connection.join(chan)

    def part(self, chan, reason=""):
        """ Parts a channel. """
        self.connection.part(chan, reason)

    def pubmsg(self, chan, msg, target=None):
        """ Sends a public message to a specified channel.
            Optionally, a user in the channel may be specified as a target, in
            which case their name and a colon will be prepended to the message.
            """
        if target:
            msg = target + ": " + msg
        self.connection.privmsg(chan, msg)
        self.logger.logself(self.connection, "pubmsg", target=chan, msg=msg)

    def privmsg(self, target, msg):
        """ Sends a private message to a specified user. """
        self.connection.privmsg(target, msg)
        self.logger.logself(self.connection, "privmsg", target, msg)

    def privnotice(self, target, msg):
        """ Sends a private notice to a specified user. """
        self.connection.notice(target, msg)
        self.logger.logself(self.connection, "privnotice", target, msg)

    ## Main event handlers ##

    def on_privmsg(self, c, e):
        return self.handle_priv(c, e, self.privmsg)

    def on_privnotice(self, c, e):
        return self.handle_priv(c, e, self.privnotice)

    def handle_priv(self, c, e, reply):
        src = e.source()
        msg = e.arguments()[0]
        if src:
            src = nm_to_n(src)
            if src.upper() == 'NICKSERV':
                return self.handle_nickserv(c, e)
            elif '.' in src or '.' in e.target():
                # Don't handle these;
                # they are messages from the server and not users
                return None
            elif (msg.lower().startswith('[global notice]')
                  or msg.lower().startswith('[server notice]')):
                # Don't handle these;
                # they are messages from the server admins
                return None
            elif self.floodcontrol.checkignore(src, src):
                return None
            else:
                mynick = c.get_nickname()
                if re.match(mynick + '[:,]', msg, re. I):
                    # if the user included the nick, snip it
                    msg = msg[len(mynick)+1:].strip()
                result = self.handle_command(c, e, src, msg, is_privmsg=True)
                if not result:
                    result = self.handle_targeted(c, e, src, msg)
                if result:
                    reply(src, result)

    def handle_command(self, c, e, src, msg, targeted=False, is_privmsg=False):
        """ Checks the message against the CommandList.
            If this function is overridden, remember to call super.evalcommand,
            or the bot's command list will not function properly. """
        target = e.target().lower()
        # commands require capab id!
        id = e.source_identified if self.capab_id else False
        if is_privmsg:
            if self.isAdmin(src, e):
                result = self.commandlist.parse_admin_command(id, src, msg)
            else:
                result = self.commandlist.parse_private_command(id, src, msg)
        else:
            result = self.commandlist.parse_public_command(id, src, target,
                                                           msg, targeted)
        return result

    def on_pubmsg(self, c, e):
        src = e.source()
        chan = e.target()
        msg = e.arguments()[0]
        if len(msg) < 1:
            return None
        if src:
            src = nm_to_n(src)
            if self.floodcontrol.checkignore(src, chan):
                return None
            result = None
            mynick = c.get_nickname()
            if chan not in self.muted or not self.muted[chan]:
                # Evaluate command
                if re.match(mynick + '[:,]', msg, re.I):
                    # No response from the command section
                    # means no command handled or responded to.
                    m = msg[len(mynick)+1:].strip()
                    result = self.handle_command(c, e, src, m, targeted=True)
                    if not result:
                        result = self.handle_targeted(c, e, src, m)
                else:
                    result = self.handle_command(c, e, src, msg)
                    if not result:
                        result = self.handle_untargeted(c, e, src, msg)
                if result and type(result) == str:
                    self.floodcontrol.ensuredelay(chan)
                    self.pubmsg(chan, result, target=src)
                    self.floodcontrol.markuser(src, chan, e)

    def handle_targeted(self, c, e, src, msg):
        """ Override to provide additional functionality to users talking to
            the bot. This function is called when someone enters a private
            message or a public message which starts with the bot's name
            followed immediately by a comma or colon. """
        return None

    def handle_untargeted(self, c, e, src, msg):
        """ Override to provide additional functionality for the bot to
            respond to conversation not directed at it.
            ** Be careful not to spam! ** """
        return None

    ## Version info ##

    def _version(self, *args):
        """ version: Returns the bot's version. """
        return self.version

    ## Admin-relevant functions ##

    def isAdmin(self, nick, e):
        if self.capab_id:
            return e.source_identified and nick in self.adminconfirmed
        return nick in self.adminconfirmed

    def alertadmin(self, msg):
        """ Sends a message to the owner of the bot
            (the first admin listed). """
        self.privmsg(self.adminlist[0], msg)

    ## admin command family ##

    def admin(self, src, chat, s, m):
        g = m.groups()
        if g[0] == "id":
            return self.admin_id(src, g[1])
        if g[0] == "add":
            return self.admin_add(src, g[1].split())
        if g[0] == "rm" or g[0] == "remove":
            return self.admin_rm(src, g[1].split())
        if self.apass and g[0] == "release":
            return self.admin_release(src)
        return "I don't understand your command."

    def not_admin(self, src, chat, s, m):
        g = m.groups()
        if g[0] == 'id':
            return self.admin_id(src, g[1])
        if src not in self.adminlist:
            return "You are not recognized as an admin."
        return "You are not identified as an admin."

    def admin_id(self, src, password):
        if src not in self.adminlist:
            return "You are not recognized as an admin."
        if src in self.adminconfirmed: # True if no admin password
            return "You are already identified as an admin."
        if password == self.apass:
            self.adminconfirmed += [src]
            return "You are now identified as an admin."
        return "Your password has not been accepted."

    def admin_release(self, src):
        self.adminlist.remove(src)
        return "You are no longer identified as an admin."

    def admin_add(self, src, names):
        added = []
        for name in names:
            if name not in self.adminlist:
                self.adminlist += [name]
                added += [name]
        if added:
            return ', '.join(added) + ' added to admin list.'
        else:
            return "Those users are already admins."

    def admin_rm(self, src, names):
        removed = []
        for name in names:
            if name in self.adminlist and name != self.adminlist[0]:
                self.adminlist.remove(name)
                removed += [name]
        if removed:
            return ', '.join(removed) + ' removed from admin list.'
        elif self.adminlist[0] in names:
            return "You cannot remove the bot owner from the admin list."
        else:
            return "None of those users are admins."

    ## Other admin commands ##

    def c_join(self, src, chat, channel):
        """ join <channel>: Tells the bot to join the specified channel. """
        if channel not in self.chanlist:
            self.chanlist += [channel]
        self.join(channel)
        return "Okay."

    def mute(self, src, chat, channel):
        """ mute <channel>: Stop responding and evaluating
            commands in the channel. """
        if channel not in self.chanlist:
            return "I'm not in %s." % channel
        self.muted[channel] = True
        msg = "%s has requested that I be silent for a while." % src
        self.privmsg(channel, msg)
        return "Okay, I'll be quiet now."

    def c_part(self, src, chat, channel):
        """ part <channel> [reason]: The bot will part the channel. """
        if channel not in self.chanlist:
            return "I'm not in %s." % channel
        self.part(channel)
        return "Okay."

    def quit(self, src, chat):
        """ quit: Shut the bot down. """
        self.stop("Shutting down.")

    def unmute(self, src, chat, channel):
        """ unmute <channel>: Returns the bot to responding and
            evaluating as normal. """
        if channel not in self.muted or not self.muted[channel]:
            return "I haven't been quieted in %s." % channel
        self.muted[channel] = False
        if channel not in self.chanlist:
            return "I'm not in %s." % channel
        self.action(channel, "removes the duct tape from his mouth.")
        return "Okay, I am no longer quiet in %s." % channel

    # In-channel temporary muting
    def channel_mute(self, src, chat, *args):
        """ Silence the bot in the channel for 5 minutes. """
        self.muted[chat] = True
        def um():
            self.muted[chat] = False
        Timer(300.0, um).start()
        return "Okay, I'll be quiet for 5 minutes."

    ## Starting and stopping the bot! ##

    def start(self):
        try:
            # Load config here rather than on init in case
            # modules are added after init
            config.load_config(self.configfile, self.modules)
            super(CommandBot, self).start()
        except SystemExit:
            raise
        except:
            self.logimportant("Unexpected error:\n" + traceback.format_exc())
            self.alertadmin("Uncaught exception: " + str(sys.exc_info()[0]))
            # attempt to save state
            config.save_config(self.configfile, self.modules)
            raise

    def stop(self, msg):
        for m in self.modules:
            self.modules[m].shutdown()
        self.connection.disconnect(msg)
        print msg
        config.save_config(self.configfile, self.modules)
        sys.exit(0)

    def shutdown(self):
        """ This is called by the stop() function. Override this function
            to perform anything needed (other than module shutdowns)
            before the bot shuts down. """
        pass

