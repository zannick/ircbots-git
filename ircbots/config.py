# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""config -- Enables an ircbot to save part of its state across reboots.

The functions here allow an ircbot to write several internal variables
across multiple modules to a config file, which will be read when the bot
starts up again, and the proper variables set to what they were set before.

This version outputs the config as a large, human-readable python dict.
Reading the config file causes this dict to be evaluated into a python dict
object. Hence, while modification of the config file is possible, an incorrectly
formed dict may cause the bot to produce errors.
"""

import bisect
import os

# A config list contains only the names of the attributes to be saved.
# The saved config file contains a mapping from those names to their saved values.

def add_config(module, attributes):
    """ Adds a list of attributes to the module's list of attributes to be saved
        when save_config is called. """
    if not hasattr(module, "config"):
        module.config = []
    for attr in attributes:
        if attr not in module.config:
            bisect.insort(module.config, attr)

def load_config(filename, modules):
    """ Loads saved variables from a configuration file, which must contain
        a string -> (string -> value dict) dict. Each of the first entries in
        the config dict are module names, and the provided modules dictionary
        should allow access to the module of that name.
        The inner dictionary will consist of attribute names as keys.
        This function will assign their values to the respective modules' attributes.

        If a value is a list, the resultant attribute will be a union
        of the saved value and the original value. If the value is a dict, the
        resultant attribute dictionary will contain the values given, plus any original
        values not overwritten.

        If a module is not present in the modules dict, the loaded values are discarded.

        If no such file exists, then no values are loaded. """
    if os.path.exists(filename):
        with open(filename, 'r') as f:
            config = eval(f.read())
            for mclass in config:
                if mclass in modules:
                    module = modules[mclass]
                    for attr in config[mclass]:
                        sval = config[mclass][attr]
                        if hasattr(module, attr):
                            val = getattr(module, attr)
                            if type(val) is list:
                                for x in sval:
                                    if x not in val:
                                        val += [x]
                                continue
                            elif type(val) is dict:
                                for x in sval:
                                    val[x] = sval[x]
                                continue
                        setattr(module, attr, sval)
                    add_config(module, config[mclass])

def _config_string(module):
    """ [Internal] Generate the dict string for a single module. """
    config = ['%s: %s' % (repr(x), getattr(module, x, None)) for x in module.config]
    s = '{\n    ' + ',\n    '.join(config) + '\n    }'
    return s

def save_config(filename, modules):
    """ Saves the attributes specified in each module's config list, in the format
        required by load_config. modules should be a dict of module names to their
        respective module objects. """
    config = ['%s: %s' % (repr(a), _config_string(b)) for a, b in modules.iteritems()]
    s = '{\n' + ',\n'.join(config) + '\n}'
    with open(filename, 'w') as f:
        f.write(s)
        
