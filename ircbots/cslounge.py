# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""A quick utility to make bots emit the correct IETFNG flags in #cslounge.
"""

from functools import partial
from commandbot import CommandBot

autoflags = ""

def _action(self, chan, msg):
    if chan == "#cslounge":
        # Add the flags to indicate this is a bot
        self.connection.action(chan, msg + autoflags)
    else:
        self.connection.action(chan, msg)
    self.logger.logself(self.connection, "action", target=chan, msg=msg)

def _pubmsg(self, chan, msg, target=None):
    if target:
        msg = target + ": " + msg
    if chan == "#cslounge":
        # Add the flags to indicate this is a bot
        self.connection.privmsg(chan, msg + autoflags)
    else:
        self.connection.privmsg(chan, msg)
    self.logger.logself(self.connection, "pubmsg", target=chan, msg=msg)

class CsloungeCommandBot(CommandBot):
    """ Overrides the CommandBot's action and pubmsg commands to ensure
        that the bot sends the IETFNG flags used to indicate that the message
        originates from a bot. """
    action = _action
    pubmsg = _pubmsg

def csloungify(bot):
    """ Instead of making your bot inherit from CsloungeCommandBot, you can
        use @csloungify to make your bot class use the proper functions anyway,
        or just call csloungify on your bot instance. """
    if type(bot) == type:
        bot.action = _action
        bot.pubmsg = _pubmsg
    else:
        bot.action = partial(_action, bot)
        bot.pubmsg = partial(_pubmsg, bot)
    return bot
