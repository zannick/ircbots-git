# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""A simple example karma bot.

This is a very basic bot that keeps track of "karma" of users on a per-channel
basis. It does not include complicated features such as rate-limiting or
linking separate nicks into one "account". The former is perhaps a good reason
not to actually use this as a karmabot. """

import os
import re
from irclib import nm_to_n
from modules.module import Module
from modules.command import RegexCommand, ALLOW

version = "KarmaBot 1.0 by bswolf"
kstring = "%s has %d karma in %s."

class Karma(Module):
    """ The karma of a user in a channel is the number of times other users
        have "upmodded" them minus the number of times other users have
        "downmodded" them. An upmod is their name followed or preceded
        immediately by "++", while a downmod is their name followed or preceded
        immediately by "--".

        This module takes the convention that all tokens in a line that are
        not the speaker's name are to be altered as appropriate. """
    def __init__(self, filename, cmdchar='!'):
        """ Karma is saved and loaded from the supplied file, as an alternative
            to bloating the config file with all the data. """
        super(Karma, self).__init__()
        self.filename = filename
        self.karma = {} # channel -> name -> score
        if os.path.exists(self.filename):
            with open(self.filename, 'r') as f:
                self.karma = eval(f.read())
        regex = re.compile(re.escape(cmdchar) + r"karma(?: (\S+)(?: (\S+))?)?", re.I)
        lookup = RegexCommand(regex, cmdchar + "karma", self.lookup)
        self.public_commands += [(lookup, ALLOW, False)]
        self.handlers += [("pubmsg", self.parse_mods)]

    def _chanstring(self, channel):
        """ [Internal] Generate the karma string for a channel. """
        c = ['%s: %s' % (n, s) for n, s in self.karma[channel].iteritems()]
        return repr(channel) + ': {\n    ' + ',\n    '.join(c) + '\n    }'

    def shutdown(self):
        # Save
        if not os.path.exists(self.filename):
            if '/' in self.filename:
                os.makedirs(self.filename[:self.filename.rindex('/')])
        c = [self._chanstring(channel) for channel in self.karma]
        k = '{\n' + ',\n'.join(c) + '\n}'
        with open(self.filename, 'w') as f:
            f.write(k)

    ## Handler ##

    def parse_mods(self, c, e):
        """ Parse the message by splitting it on spaces, then each "word"
            is determined to be an upmod, a downmod, or neither. The same
            token cannot be changed more than once in the same direction,
            and the source cannot modify themselves at all. """
        src = nm_to_n(e.source()).lower()
        chan = e.target().lower()
        msg = e.arguments()[0].lower()
        tokens = msg.split()
        up = []
        down = []
        for t in tokens:
            if t.startswith('++'):
                up += [t[2:]]
            elif t.endswith('++'):
                up += [t[:-2]]
            elif t.startswith('--'):
                down += [t[2:]]
            elif t.endswith('--'):
                down += [t[:-2]]
        if chan not in self.karma:
            self.karma[chan] = {}
        for t in set(up):
            if t not in self.karma[chan]:
                self.karma[chan][t] = 0
            if t != src:
                self.karma[chan][t] += 1
        for t in set(down):
            if t not in self.karma[chan]:
                self.karma[chan][t] = 0
            if t != src:
                self.karma[chan][t] -= 1

    ## Lookup command ##

    def lookup(self, src, target, fullcommand, match):
        """ karma <name> [channel]: Look up the karma associated with <name>.
            If name is unspecified, it is assumed to be the source.
            If channel is unspecified, it is assumed to be the current channel.
            You must specify channel in a private message. """
        name, channel = match.groups()
        if channel is None:
            if src == target:
                if name and name[0] == '#':
                    channel = name
                    name = src
                else:
                    return "You must specify a channel."
            else:
                channel = target
        if name is None:
            name = src
        c = channel.lower()
        n = name.lower()
        if c not in self.karma:
            return "I don't know anything about %s." % channel
        if n not in self.karma[c]:
            return kstring % (name, 0, channel)
        return kstring % (name, self.karma[c][n], channel)


if __name__ == "__main__":
    from commandbot import CommandBot
    # You should replace these arguments with your own, of course,
    # especially the channel and admin lists
    karmabot = CommandBot('irc.freenode.net', 6667, [], 'zkarmabot',
            ['Zannick'], modules=[Karma(".karma")])
    karmabot.start()
