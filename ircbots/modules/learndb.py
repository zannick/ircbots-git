# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""learndb -- a modifiable database through which users can look up
definitions.

This Module provides an ircbot with the ability to maintain a learndb through
a Redis database. The learndb can be accessed through private message, but it
can only be modified in channel. """

import json
import re
from redis import RedisError
from command import CharTriggerCommand, RegexCommand, ALLOW
from module import InstArg, ircbot_module, public_command
from mredis import Redis

version = "LearnDB 0.5 by bswolf"

# Strings
termnotfound = "Term '%s' is not in my dictionary."
indexnotfound = "I don't seem to have an entry for %s[%d]."
delsuccess = "Removed term %s[%d]: %s."
sillything = "That seems like a silly thing for me to do."
swapsuccess = "Terms %s[%d] and %s[%d] successfully swapped."
badregex = "I don't like your regular expression."
terminfo = "Term '%s' contains contributions by: %s."
contribinfo = "Term %s[%d] was contributed by %s."
printterm = "%s[%d/%d]: %s"

# Keys
termkey = "learn:%s" # LIST of JSON encoded strings
termform = '{"term": "%s", "contributor": "%s"}'

# Errors
moreparams = "Insufficient parameters for %s."
needindex = "I need an index with that term."
unknownerror = "LearnDB: Unknown error in %s: %s"

subregex = re.compile(r"^s(.)(.+)\1(.*)\1(gi|ig|g|i|)$")
redirregex = re.compile(r"^[sS]ee \{(.*)\}$")
termindexregex = re.compile(r"(.+)\[(\d+)\]")

def applyregex(str, regex):
    """ Apply a perl-style regex in the form s/FIND/REPLACE/ to a string.
        Available options are 'g', for replacing all instances of 'find'
        with 'replace', and 'i' for ignoring case.

        The forward slashes used to separate the find and replace strings
        can be replaced with a different character, in case find and/or
        replace may contain forward-slashes and the user does not want to
        deal with backslashes.
        
        Returns a triple of arguments, the first of which indicates whether
        the operation was a success. The second is the new string, if
        the operation was successful, or an error message otherwise.
        The third is the number of replacements made, if the operation was
        successful, or 0. """
    try:
        sedit = subregex.match(regex)
        if sedit is None:
            return (False, badregex, 0)
        (sep, find, repl, gi) = sedit.groups()
        if 'i' in gi:
            rg = re.compile(find, re.I)
        else:
            rg = re.compile(find)
        if 'g' in gi:
            count = 0
        else:
            count = 1
        (vs, numrep) = rg.subn(repl, str, count)
        return (True, vs, numrep)
    except re.error:
        return (False, badregex, 0)

@ircbot_module
class LearnDB(Redis):
    """ A database that maps terms to a series of definitions. """
    def __init__(self, host='localhost', port=6379, db=0,
                 password=None, socket_timeout=None, log=None, cmdchar='!'):
        """ Creates a Redis client for the specified server.
            log - optional function to log important errors and failures.
                  It should take a single string as an argument.
            cmdchar - The command character for the commands this class
                      defines. """
        self.cmdchar = cmdchar # done by Redis, but just to be sure
        super(LearnDB, self).__init__(host, port, db, password,
                                      socket_timeout, log, cmdchar)
        # As swap cannot simply use a pipeline to guard against simultaneous
        # deletes, we have to use a lock to ensure that deletes and swaps are
        # atomic with respect to each other.
        # edit will also need to lock to prevent delete from eating an element
        # causing edit to modify an element off the end of the list
        self.lock = self.redis.lock("learndb_lock")

    @public_command(CharTriggerCommand, InstArg("cmdchar"), "learn",
                    [str, str], pm_option=ALLOW, triggered=False)
    def learn(self, src, chat, cmd, data):
        """ learn <command> <args>: Interact with the learndb.
            Commands include:
                add <term> <text>,
                del <term[index]>,
                edit <term[index]> s/replace-this/with-this/,
                query <term[index]>,
                swap <term[index]> <term[index]>.
            For del and edit, the index must be included with the term.
            For swap, the index must be included for both. """
        if cmd not in ["add", "del", "edit", "query", "swap"]:
            return "That's not a valid learndb command."
        if cmd in ["add", "del", "edit", "swap"] and src == chat:
            return "The learndb can only be modified in channel."
        if cmd == "add":
            params = data.split(' ', 1)
            if len(params) < 2:
                return moreparams % "learn add"
            # If the term provided includes an index, chop it out.
            m = termindexregex.match(params[0])
            term = m and m.group(1) or params[0]
            return self.add(term, params[1], src)
        elif cmd == "del":
            if not data:
                return moreparams % "learn del"
            # this can include spaces as long as it includes an index
            m = termindexregex.match(data)
            if m:
                term, index = m.groups()
                term = term.replace(' ', '_')
                return self.delete(term, index)
            else:
                return needindex
        elif cmd == "edit":
            params = data.split(' ', 1)
            if len(params) < 2:
                return moreparams % "learn edit"
            m = termindexregex.match(params[0])
            if m:
                term, index = m.groups()
                return self.edit(term, index, params[1])
            else:
                return needindex
        elif cmd == "query":
            if not data:
                return moreparams % "learn query"
            # this can include spaces
            m = termindexregex.match(data)
            term, index = m and m.groups() or (data, 1)
            term = term.replace(' ', '_')
            return self.query(term, index)
        elif cmd == "swap":
            params = data.split(' ', 1)
            if len(params) < 2:
                return moreparams % "learn swap"
            m = termindexregex.match(params[0])
            n = termindexregex.match(params[1])
            if m and n:
                terma, indexa = m.groups()
                termb, indexb = n.groups()
                return self.swap(terma, indexa, termb, indexb)
            else:
                return needindex

    @public_command(RegexCommand, re.compile(r"\?\? ?(.*)"), "/learn_lookup",
                    pm_option=ALLOW, targeted=False)
    def lookup(self, src, chat, line, match):
        g = match.group(1)
        if g:
            s = g.strip().replace(' ', '_')
            m = termindexregex.match(s)
            term, index = m and m.groups() or (s, 1)
            return self.query(term, index)

    def add(self, term, defn, user):
        """ Adds a definition for a term.
            The new definition is added to the next available index
            within the specific term, along with the nickname
            of the contributor. """
        try:
            index = self.redis.rpush(termkey % term, termform % (defn, user))
            return printterm % (term, index, index, defn)
        except RedisError as e:
            self.log(unknownerror % ("add", e))

    def delete(self, term, index):
        """ Deletes an entry from the dictionary.
            The term itself is only removed if no entries exist. """
        try:
            with self.lock:
                l = self.redis.llen(termkey % term)
                if l == 0:
                    return termnotfound % term
                elif index < 1 or l < index:
                    return indexnotfound % (term, index)
                # Change it to a sentinel value then delete it
                # We want to grab and delete atomically
                p = self.redis.pipeline(transaction=True)
                p.lindex(termkey % term, index - 1)
                p.lset(termkey % term, index - 1, "DELETEME")
                p.lrem(termkey % term, 1, "DELETEME")
                results = p.execute()
                for a in results:
                    if isinstance(a, Exception):
                        self.log(unknownerror % ("delete-atom", a))
                defn = results[0]
                return delsuccess % (term, index, defn)
        except RedisError as e:
            self.log(unknownerror % ("delete", e))

    def swap(self, terma, indexa, termb, indexb):
        """ Swaps two entries in the dictionary. """
        try:
            with self.lock:
                la = self.redis.llen(termkey % terma)
                if la == 0:
                    return termnotfound % terma
                lb = self.redis.llen(termkey % termb)
                if lb == 0:
                    return termnotfound % termb
                if terma == termb and indexa == indexb:
                    return sillything
                if indexa < 1 or indexa > la:
                    return indexnotfound % (terma, indexa)
                if indexb < 1 or indexb > lb:
                    return indexnotfound % (termb, indexb)
                a = self.redis.lindex(termkey % terma, indexa - 1)
                b = self.redis.lindex(termkey % termb, indexb - 1)
                p = self.redis.pipeline(transaction=True)
                p.lset(termkey % terma, indexa - 1, b)
                p.lset(termkey % termb, indexb - 1, a)
                p.execute()
                return swapsuccess % (terma, indexa, termb, indexb)
        except RedisError as e:
            self.log(unknownerror % ("swap", e))

    def edit(self, term, index, regex):
        """ Edit an entry in the dictionary by using a perl-style regex,
            s/FIND/REPLACE/, where the entry is matched against the FIND
            regex. 'g' and/or 'i' can be added to the end of the regex. """
        try:
            with self.lock:
                l = self.redis.llen(termkey % term)
                if l == 0:
                    return termnotfound % term
                if index < 1 or index > l:
                    return indexnotfound % (term, index)
                old = json.loads(self.redis.lindex(termkey % term, index - 1))
                (success, result, numrep) = applyregex(old["term"], regex)
                if not success: # Error
                    return result
                old["term"] = result
                self.redis.lset(termkey % term, index - 1, json.dumps(old))
                return printterm % (term, index, l, result)
        except RedisError as e:
            self.log(unknownerror % ("edit", e))

    def info(self, term):
        """ Returns information about the contributors to a term
            in the dictionary. There are no records of edits. """
        try:
            dlist = map(json.loads, self.redis.lrange(termkey % term, 0, -1))
            if len(dlist) == 0:
                return termnotfound % term
            contributors = {}
            cntrb = map(lambda x:x["contributor"], dlist)
            for user in cntrb:
                if user in contributors:
                    contributors[user] += 1
                else:
                    contributors[user] = 1
            # sort list descending by number of contributions
            contributors = sorted(contributors.items(),
                    key=lambda (c,d): d, reverse=True)
            contrib_info = map(lambda x:"%s (%d)" % x, contributors)
            return terminfo % (term, ', '.join(contrib_info))
        except RedisError as e:
            self.log(unknownerror % ("info", e))

    def getcontrib(self, term, index):
        """ Returns who contributed this entry. """
        try:
            l = self.redis.llen(termkey % term)
            if l == 0:
                return termnotfound % term
            if index < 1 or index > l:
                return indexnotfound % (term, index)
            d = json.loads(self.redis.lindex(termkey % term, index - 1))
            return contribinfo % (term, index, d["contributor"])
        except RedisError as e:
            self.log(unknownerror % ("getcontrib", e))

    def isredirect(self, term):
        """ Determines whether the dictionary entry for term
            is a redirect to another entry. Only checks the first entry. """
        try:
            if self.redis.llen(termkey % term) == 0:
                return None
            d = json.loads(self.redis.lindex(termkey % term, 0))
            redir = redirregex.match(d["term"])
            if redir:
                return redir.group(1)
            return None
        except RedisError as e:
            self.log(unknownerror % ("isredirect", e))

    def query(self, term, index):
        """ Performs a lookup, following at most 5 redirects. """
        try:
            for depth in range(5):
                term2 = self.isredirect(term)
                if term2:
                    term = term2
                else:
                    break
            l = self.redis.llen(termkey % term)
            if l == 0:
                return termnotfound % term
            if index < 1 or index > l:
                return indexnotfound % (term, index)
            d = json.loads(self.redis.lindex(termkey % term, index - 1))
            return printterm % (term, index, l, d["term"])
        except RedisError as e:
            self.log(unknownerror % ("query", e))
            

