# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""floodcontrol -- Controls to prevent the bot from spamming channels.

This Module contains a set of tools to prevent the bot from being
repeatedly triggered in one channel in a short span of time.
"""

import time
from threading import Timer
from module import Module, InstArg, ircbot_module, admin_command
from command import CharTriggerCommand

version = "Flood Control 0.5 by bswolf"

permabanned = "You have been permanently banned for repeated flood " + \
              "triggering. If you feel this is without merit, please " + \
              "contact one of my admins."
ignored = "You are being ignored for %d minutes for flood triggering."

@ircbot_module
class FloodControl(Module):
    """ This class supplies functions for preventing the bot from being
        overly chatty or spammy as a direct consequence of a user invoking a
        command or otherwise talking to the bot.
        
        A user that causes the bot to respond in one channel a certain number
        of times in a small enough period of time will receive a strike and
        will be ignored (for command and response purposes) for a specified
        period of time. A strike is removed after 24 hours of not receiving
        a strike. When a user receives more than the maximum allowed strikes,
        they are permanently ignored and must consult an admin to have the ban
        lifted.
        
        While responses are counted separately by channel,
        strikes are per-user.

        Additionally, a bot can have channels where the above does not apply.

        The bot should invoke the ensuredelay() function before sending public
        responses. This will ensure that sufficient time has passed since the
        last response before sending the next.
        
        This module includes commands to manually ignore a user (or other bot),
        unignore an ignored user, or list all ignored users. """
    def __init__(self, isAdmin, privmsg, responsedelay=3, maxmarks=5,
            mintime=120, ignoretimes=(5,30), freechannels=(), cmdchar='!'):
        """ isAdmin - the bot's (nick, event) function that determines whether
                      a user is an admin of the bot (admins are never marked).
            privmsg - a function (nick, msg) that will send a private message
                      to the specified user
            responsedelay - The minimum number of seconds between bot responses
                            in public.
            maxmarks - number of bot responses caused in mintime before
                       getting a strike
            mintime - period of time (seconds) to watch for multiple responses
            ignoretimes - A list of times (in minutes) to ignore a user who
                          previously had n strikes. (So a list of [5, 15] would
                          ignore a user for 5 minutes on their first strike,
                          then 15 on their second.) When a user has more
                          strikes than elements in this list, they are
                          permanently ignored.
            freechannels - A list of channels in which users won't be marked.
            cmdchar - the command character for this module's commands. """
        self.cmdchar = cmdchar
        super(FloodControl, self).__init__()
        self.isAdmin = isAdmin
        self.privmsg = privmsg
        self.responsedelay = responsedelay
        self.maxmarks = maxmarks
        self.mintime = mintime
        self.ignoretimes = ignoretimes
        self.maxstrikes = len(ignoretimes)
        self.freechannels = freechannels
        self.lastresponse = {} # channel -> time (float)
        self.recent = {} # channel -> nick -> responses caused in prev. mintime
        self.strikes = {} # nick -> (number of strikes, time of last strike)
        self.ignorelist = [] # nick list
        # Don't save recent marks; a bot reset should be more than enough time
        self.config += ["ignorelist", "lastresponse", "strikes"]

    def ensuredelay(self, channel):
        """ Ensures the bot waits a proper length of time before sending
            another response to the given channel. This assumes that such a
            response occurs. """
        channel = channel.lower()
        t = time.time()
        if channel in self.lastresponse:
            b = self.lastresponse[channel]
            while b + self.responsedelay > t:
                time.sleep(b + self.responsedelay - t)
                t = time.time()
        else:
            self.lastresponse[channel] = t

    def checkignore(self, nick, channel):
        """ Checks whether the given user should be ignored this
            particular time. """
        channel = channel.lower()
        nick = nick.lower()
        if nick not in self.ignorelist:
            return False
        if nick not in self.strikes:
            # Permabans are in ignorelist but not in strikes
            return True
        if channel in self.freechannels:
            return False
        # Check whether the ignore ended
        nstrikes, last = self.strikes[nick]
        delay = 60 * self.ignoretimes[nstrikes - 1]
        if last + delay < time.time():
            # Unignore
            self.ignorelist.remove(nick)
            return False
        return True

    def markuser(self, nick, channel, e):
        """ Note that a user has caused a bot response in a channel. If they
            have caused the maximum allowed in the specified time, they are
            given a strike. """
        if self.isAdmin(nick, e):
            return None
        if channel in self.freechannels:
            return None
        nlower = nick.lower()
        channel = channel.lower()
        if channel not in self.recent:
            self.recent[channel] = {nlower: 1}
        elif nlower not in self.recent[channel]:
            self.recent[channel][nlower] = 1
        else:
            self.recent[channel][nlower] += 1
        def unmark():
            self.recent[channel][nlower] -= 1
        Timer(self.mintime, unmark).start()
        if self.recent[channel][nlower] >= self.maxmarks:
            # Give user a strike
            self.givestrike(nick)

    def givestrike(self, nick):
        """ Gives a user a strike, puts them on the ignore list for the
            appropriate amount of time, and informs them via private message.
            The strike will go away after 24 hours without a strike. """
        nlower = nick.lower()
        t = time.time()
        self.ignorelist += [nlower]
        if nlower not in self.strikes:
            a = 0
        else:
            a, b = self.strikes[nlower]
            # This is a lazy remove of strikes,
            # in case the bot is not up for 24 hours
            while a > 0 and (b + 86400) < t:
                a -= 1
                b += 86400
            if a >= self.maxstrikes:
                # They already have the max number of strikes, permaban them.
                del self.strikes[nlower]
                self.privmsg(nick, permabanned)
                return None
        self.strikes[nlower] = (a+1, t)
        self.privmsg(nick, ignored % (self.ignoretimes[a]))

    ## Commands ##

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "ignore", [str])
    def ignore(self, src, chat, nick):
        """ ignore <nick>: Prevents response-generating triggers on any
            messages from a particular user. The user is still logged where
            applicable. """
        self.ignorelist += [nick.lower()]
        return "Now ignoring %s." % nick

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "unignore", [str])
    def unignore(self, src, chat, nick):
        """ unignore <nick>: Removes a user from the ignore list. """
        nlower = nick.lower()
        if nlower in self.ignorelist:
            self.ignorelist.remove(nlower)
            return "%s has been unignored." % nick
        return "I was not ignoring %s." % nick

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "getignorelist", [])
    def getignorelist(self, src, chat):
        """ getignorelist: Lists users that are ignored. """
        return "Ignoring: " + ', '.join(self.ignorelist) + '.'
