# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""command -- Basic command types and the CommandList.

This module contains the CommandList Module which every CommandBot
will have, as well as three basic Command types: the base class itself,
and RegexCommand and CharTriggerCommand. These latter two will likely be
sufficient for most cases, while the former is intended only to be an
abstract superclass.

The primary feature of this class is that it reduces the verbiage necessary
to get the bot to respond in the desired manner. All that is necessary is 
a call to the proper add_command function in the CommandList, rather than
adding several lines into a large if...elif...else block. (You still have
to define the function that the command uses.)
"""

import re
import bisect

version = "CommandList 0.5 by bswolf"

intregex = r"[-+]?\d+"
longregex = r"[-+]?\d+L?"
floatregex = r"[-+]?\d+|[-+]?\d*\.\d+"
complexregex = r"[-+]?\d+|[-+]?(?:\d+[-+])?\d*j"
strregex = r"[^ ]+"
listregex = ".*"

typeregexes = { int: intregex, long: longregex, float: floatregex,
        complex: complexregex, str: strregex, list: listregex }

class CommandError(Exception):
    pass

class Command(object):
    """ Represents a command a bot may use, and allows parsing of a command
        given to a bot to be easily evaluated. At its core, a command consists
        of a name, a function that performs the actual handling and returns a
        string response, a "help" description of the command, and whether the
        Command can only be invoked by identified users. The arguments to the
        function will all be passed as unnamed arguments.

        The nick of the user and the context of the command
        (channel, or the nick of the user again if a private message)
        are sent as the first and second arguments, respectively,
        in the function call (src, followed by chat).
        Ignore these arguments for the purposes of argtypes.

        For arguments which could be multiple types, it is advisable to have
        them passed as strings and handled within the function provided.

        Optionally a help string can be provided, either through the help
        argument or the docstring of the provided function. The help argument
        will take precedence. """
    def __init__(self, name, fn, req_id=False, help=None):
        if not callable(fn):
            raise CommandError("fn must be callable type: " + str(type(fn)))
        self.name = name
        self.fn = fn
        self.req_id = req_id
        if not help and fn.__doc__:
            self.help = ' '.join([x.strip() for x in fn.__doc__.split('\n')])
        else:
            self.help = help

    def eval(self, id, src, chat, *args):
        """ Calls the evaluation function with the requisite arguments.
            If the user needs to be identified and isn't, returns an error
            message without evaluating the command. """
        if self.req_id and not id:
            return "You must be identified to use this command."
        return self.fn(src, chat, *args)

    def match(self, command):
        """ Checks whether the given command string should trigger
            this command. If it does, returns a nonempty tuple of arguments
            that should be passed to eval (in addition to id, src, and chat),
            otherwise returns None. """
        if command.startswith(self.name):
            return (command,)
        return None

class RegexCommand(Command):
    """ Represents a particular type of command a bot may use,
        namely that which is triggered by a match to a regular expression.

        When a match occurs to cause evaluation of this command,
        the arguments passed to the evaluation function are the source
        of the command, the chat (channel) the command occurred in,
        the string matched, and the entire match object. """
    def __init__(self, regex, name, fn, req_id=False, help=None):
        """ Creates a Command object with a regex trigger.
            regex - a compiled regular expression.
            name - the name of this command - should be unique.
            fn - a callable function that can accept src, chat, the full
                 string, and a match object as unnamed arguments, in that order
            req_id - whether the user must be identified to use this command
            help - an optional help string for this command. """
        self.regex = regex
        super(RegexCommand, self).__init__(name, fn, req_id, help)

    def match(self, command):
        m = self.regex.match(command)
        if m:
            return (command, m)
        return None

class CharTriggerCommand(RegexCommand):
    """ A CharTriggerCommand defines a special type of command
        consisting additionally of a trigger (one or more control characters),
        and a list of argument types (int, long, float, complex, str, and list
        [which can only appear last and is always a list of strings]).
        str and list, as the final argument, will both use the remainder of the
        line, but list will pass it already split across spaces. Note that a
        final argument that is a str or a list can be empty.
        
        Recall that the nick of the user and the context of the command
        are always sent as the first and second arguments, respectively,
        in the function call (src, followed by chat).
        Ignore these arguments for the purposes of argtypes.

        This class is implemented on top of RegexCommand by translating
        the trigger, name, and argument types into a single regular expression.
        """
    def __init__(self, trigger, name, argtypes, fn, req_id=False, help=None):
        for t in argtypes:
            if t not in typeregexes:
                raise CommandError("Unsupported type: " + str(t))
        if list in argtypes[:-1]:
            raise CommandError("List must be last arg if any.")
        self.numargs = len(argtypes)
        self.argtypes = argtypes
        rparts = ["(%s)" % typeregexes[t] for t in argtypes]
        if argtypes and argtypes[-1] in [str, list]:
            rparts[-1] = "?(.*)"
        rparts = [re.escape(trigger) + name] + rparts
        regex = ' '.join(rparts)
        # The name of a character trigger command is its trigger
        # plus the passed name!
        super(CharTriggerCommand, self).__init__(re.compile(regex),
                trigger + name, fn, req_id, help)

    def match(self, command):
        match = self.regex.match(command)
        if match:
            g = match.groups()
            args = [self.argtypes[i](g[i]) for i in range(self.numargs)]
            if self.argtypes and self.argtypes[-1] is list:
                if g[-1]:
                    args[-1] = g[-1].strip().split(' ')
                else:
                    args[-1] = []
            return args
        return None

nohelpentry = "I don't have a help entry for that command."
nocommand = "I don't recognize that command."

# Options for making public commands private
ALLOW = 0 # copy the command to the private list
DISALLOW = 1 # prevent the command from being used in private (return error)
IGNORE = 2 # do nothing; no additional command can be defined

class _blist(list):
    """ Binary sorted list """
    def __contains__(self, x):
        a = bisect.bisect_left(self, x)
        if a < len(self):
            return x == self[a]
        return False

# Doesn't use the module's special syntax for defining commands, because
# that syntax relies indirectly on this. (And this is not strictly a Module.)

class CommandList(object):
    """ Creates a simple interface that allows bots to easily organize and call
        the various commands they should accept.

        This class has three special reserved commands:
        'help' which returns the help entries of the other commands.
        'cmdinfo' which returns the full list of valid non-admin commands.
        'acmdinfo' which returns the full list of valid admin-only commands,
        and is itself an admin-only command.
        
        A CommandList is actually comprised of multiple lists, one for each of
        the four major types of situations: public (channel) commands, public
        targeted commands (those activated by including the name of the bot and
        a colon or comma before the command), private (privmsg) commands, and
        admin commands. Targeted commands will have the name of the bot
        stripped out before the commands are evaluated.

        Commands will be searched for a match based on the situation, in order
        from most restrictive to least restrictive. If a user is talking to the
        bot in a channel, the public targeted commands will be searched first,
        then the public commands. If an admin is talking to a bot in a private
        message, the admin commands will be searched first, then the private 
        targeted commands, then the private commands.

        A command allowable in public can also be allowed in private. An option
        in adding public commands controls this. By default it is set to ALLOW,
        where the same command is put in both public and private lists. Another
        option is DISALLOW, which will prevent use in private (this option
        actually creates a pseudocommand informing the user that the command
        can only be used in a channel). IGNORE will tell the list to do
        nothing; another command with that name can be added to the private
        list.

        Commands must be uniquely named, as the name of a command is used
        primarily for the help command. Commands with the same "name" but
        different command characters have different names; a
        CharTriggerCommand's name, as checked by the list, is the trigger
        followed by the "name" passed to the CharTriggerCommand.

        Adding a command with the same name as a previous command may overwrite
        the older command.

        If the name of a targeted command is prefixed with an
        forward slash (/), it will not be listed with cmdinfo or acmdinfo.
        Note this also means you should not use / as a command character.
        (You should not do this anyway.) """
    def __init__(self, cmdchar, pm_only):
        """ Initialize a CommandList. The first argument should be the command
            trigger for the 'help', 'cmdinfo', and 'acmdinfo' commands, and
            the second argument indicates whether the former two commands
            should be allowable only in PM. """
        self.public_commands = {}
        self.public_targeted = {}
        self.private_commands = {}
        self.admin_commands = {}
        # Binary sorted command name lists
        self.clist = _blist([])
        self.aclist = _blist([])
        helpstr = self.help.__doc__ % cmdchar
        helpstr = ' '.join([x.strip() for x in helpstr.split('\n')])
        help = CharTriggerCommand(cmdchar, "help", [str], self.help, help=helpstr)
        # separation for easy prevention of non-admin access to admin commands
        ahelpstr = self.ahelp.__doc__ % (cmdchar, cmdchar)
        ahelpstr = ' '.join([x.strip() for x in ahelpstr.split('\n')])
        ahelp = CharTriggerCommand(cmdchar, "help", [str], self.ahelp, help=ahelpstr)
        cmdinfo = CharTriggerCommand(cmdchar, "cmdinfo", [], self.cmdinfo)
        acmdinfo = CharTriggerCommand(cmdchar, "acmdinfo", [], self.acmdinfo)
        if pm_only:
            self.add_private_command(help)
            self.add_private_command(cmdinfo)
        else:
            self.add_public_command(help, pm_option=ALLOW)
            self.add_public_command(help, pm_option=ALLOW)
        self.admin_commands[ahelp.name] = ahelp
        self.admin_commands[acmdinfo.name] = acmdinfo
        bisect.insort(self.aclist, ahelp.name)
        bisect.insort(self.aclist, acmdinfo.name)
    
    def add_admin_command(self, command):
        """ Adds an admin command to the list. """
        name = command.name.lower()
        self.admin_commands[name] = command
        if name not in self.aclist:
            bisect.insort(self.aclist, name)

    def add_private_command(self, command):
        """ Adds a private command to the list. """
        name = command.name.lower()
        self.private_commands[name] = command
        if name not in self.clist:
            bisect.insort(self.clist, name)

    def add_public_command(self, command, pm_option=ALLOW, targeted=False):
        """ Adds a public command to the list. If pm_option is ALLOW, allows
            the command to also be invoked in a private message. If it's
            DISALLOW, returns an error upon private invocation. If it's
            anything else, no action is taken. """
        name = command.name.lower()
        command.pm_option = pm_option
        if targeted:
            self.public_targeted[name] = command
        else:
            self.public_commands[name] = command
        if pm_option is ALLOW:
            self.private_commands[name] = command
        elif pm_option is DISALLOW:
            eval = lambda *args: "This command must be used in a channel."
            c = Command(name, eval)
            c.eval = eval
            c.match = command.match
            self.private_commands[name] = c
        # else do nothing
        if name not in self.clist:
            bisect.insort(self.clist, name)

    def match_admin_command(self, line):
        """ Without evaluating any command, determine whether a line would
            match a command in the list. Returns the name of the command, or
            None if no command is matched. """
        for name, cmd in self.admin_commands.iteritems():
            if cmd.match(line):
                return name
        return self.match_private_command(line)

    def match_private_command(self, line):
        """ Without evaluating any command, determine whether a line would
            match a command in the list. Returns the name of the command, or
            None if no command is matched. """
        for name, cmd in self.private_commands.iteritems():
            if cmd.match(line):
                return name
        return None

    def match_public_command(self, line, targeted=False):
        """ Without evaluating any command, determine whether a line would
            match a command in the list. Returns the name of the command, or
            None if no command is matched. """
        if targeted:
            for name, cmd in self.public_targeted.iteritems():
                if cmd.match(line):
                    return name
        for name, cmd in self.public_commands.iteritems():
            if cmd.match(line):
                return name
        return None

    def parse_admin_command(self, id, src, line):
        """ Given a line uttered by an admin in a private conversation,
            parses it to see if it is an admin command or a private command.
            If it is either, processes it and returns a response from the bot.
            If it is not, returns None. """
        for cmd in self.admin_commands.values():
            args = cmd.match(line)
            if args is not None:
                return cmd.eval(id, src, src, *args)
        return self.parse_private_command(id, src, line)

    def parse_private_command(self, id, src, line):
        """ Given a line uttered in a private conversation, parses it to see if
            it is a private command. If so, processes it and returns a response
            from the bot. If it is not, returns None. """
        for cmd in self.private_commands.values():
            args = cmd.match(line)
            if args is not None:
                return cmd.eval(id, src, src, *args)
        return None

    def parse_public_command(self, id, src, channel, line, targeted=False):
        """ Given a line uttered in a channel, if it was targeted at the bot,
            parses it to see if it was a targeted command or a public command.
            If it was not targeted at the bot, parses only against public
            commands. If a command matches, processes it and returns a response
            from the bot. If not, returns None. """
        if targeted:
            for cmd in self.public_targeted.values():
                args = cmd.match(line)
                if args is not None:
                    return cmd.eval(id, src, channel, *args)
        for cmd in self.public_commands.values():
            args = cmd.match(line)
            if args is not None:
                return cmd.eval(id, src, channel, *args)
        return None

    ### Commands ###

    def help(self, src, chat, command):
        """ help <command>: Displays help on a command.
            For a list of commands, see %scmdinfo. """
        command = command.lower()
        opt = []
        if command in self.public_commands:
            c = self.public_commands[command]
            if c.pm_option is not ALLOW:
                opt += ["Channel-only"]
        elif command in self.public_targeted:
            c = self.public_targeted[command]
            if c.pm_option is not ALLOW:
                opt += ["Channel-only"]
            opt += ["targeted"]
        elif command in self.private_commands:
            c = self.private_commands[command]
            # PM-only as otherwise we would have found it before
            opt += ["PM-only"]
        else:
            return nocommand
        h = c.help
        if h:
            if c.req_id:
                opt += ["must be ID'ed"]
            if opt:
                h += " (%s)" % (', '.join(opt))
            return h
        else:
            return nohelpentry

    def ahelp(self, src, chat, command):
        """ help <command>: Displays help on a command.
            For a list of commands, see %scmdinfo.
            For a list of admin commands, see %sacmdinfo. """
        command = command.lower()
        if command in self.admin_commands:
            c = self.admin_commands[command]
            if c.help:
                return c.help + " (Admin-only)"
            else:
                return nohelpentry
        else:
            return self.help(src, chat, command)

    def cmdinfo(self, src, target):
        """ cmdinfo: Lists all available commands. """
        return ", ".join([x for x in self.clist if x[0] != '/'])
    
    def acmdinfo(self, src, target):
        """ acmdinfo: Lists all admin commands. """
        return ", ".join([x for x in self.aclist if x[0] != '/'])

