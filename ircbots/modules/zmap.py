# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""zmap -- IRC interface for zmap script.

zmap (and cmap and vmap) is a script used to see who is logged onto
machines in a particular cluster on Carnegie Mellon's campus. This Module
provides IRC interaction with the script (which does not itself have to be
run from a campus machine). """

import os
import re
import socket
import sys
import time
from command import RegexCommand, ALLOW
from module import Module, ircbot_module, admin_command, public_command

# the id as 3 to 8 alphanumericals is specific to CMU
nickmapregex = re.compile(r"nickmap ([a-z0-9]{3,}) (\S+)", re.I)
zmapregex = re.compile(r"(?:who(?:'s| is) in|[zc]?map) ([a-z0-9_.-]+)\??", re.I)
whichregex = re.compile(r"which(?: of those)?(?: machines)? "
                        r"(?:were|are|is) ([^\s?]+)\??", re.I)
whereregex = re.compile(r"(?:where) (?:is|am) (\w+)(?: logged [io]n)? "
                        r"in ([a-z0-9_.-]+)\??", re.I)

nocluster = "I don't recognize '%s' as a cluster."
noonethere = "I don't see anyone in %s."
clusterlings = "In %s I see: %s."

nomapping = "I don't remember mapping recently."
pastnomachines = "I didn't see %s in my last mapping."
pastmachinelings = "I saw %s on: %s."
nomachines = "I don't see %s in %s."
machinelings = "I see %s on: %s."

@ircbot_module
class Zmap(Module):
    """ Interface to the zmap script. """
    def __init__(self, zmapdir):
        """ Creates a zmap interface.
            zmapdir - string name of the directory holding zmap. """
        super(Zmap, self).__init__()
        self.zmapdir = zmapdir
        self.nickmaps = {}
        self.config += ["nickmaps"]
        self.cl_time = 0
        self.hl_time = 0
        # per-channel last map dict
        self.last_map = {}
        self.last_users = {}
        self.check_reload()

    ## Mapping ids (clusternames) to in-channel nicknames ##

    @admin_command(RegexCommand, nickmapregex, "nickmap")
    def nickmap(self, src, chat, line, match):
        clustername, nickname = match.groups()
        self.nickmaps[clustername] = nickname
        return "Okay, mapped %s to %s." % (clustername, nickname)

    def _mapnick(self, clustername):
        return self.nickmaps.get(clustername, clustername)

    def _getuser(self, src, nick):
        """ Return a reply name and a canonical name for the user.
            src is the nick of the user who invoked the command that needs
            this, and nick is the name they sent. """
        if nick.lower() in ['i', 'me']:
            return 'you', self._mapnick(src)
        else:
            return nick, self._mapnick(nick)

    ## persistent data ##

    def load_clusters(self, cl):
        _clusters = {} # cluster -> lists of canonical cluster names
        with open(cl, 'r') as f:
            for line in f:
                if '#' in line:
                    line = line[:line.index('#')]
                a = line.strip().split()
                if len(a) > 1:
                    _clusters[a[0]] = a[1]
        # some "clusters" are lists of other clusters
        # grab their canonical names
        clusters = {}
        for a, b in _clusters.items():
            if ',' in b:
                c = b.split(',')
                d = []
                for x in c:
                    if x in _clusters:
                        d.append(_clusters[x])
                    elif x in _clusters.values():
                        d.append(x)
                    else:
                        print "Unknown cluster in cluster_list:", x
                clusters[a.lower()] = d
            else:
                clusters[a.lower()] = [b]
        self.clusters = clusters

    def load_hosts(self, hl):
        hosts = {} # cluster -> (ip, hostname [short])
        for cl in self.clusters.values():
            for c in cl:
                hosts.setdefault(c, [])
        with open(hl, 'r') as f:
            for line in f:
                if '#' in line:
                    line = line[:line.index('#')]
                a = line.split()
                if len(a) > 2:
                    ip, hostname, cluster = a[:3]
                    shortname = hostname[:hostname.find('.')]
                    # we might have old defs for removed clusters, ignore them
                    if cluster in hosts:
                        hosts[cluster].append((ip, shortname))
        self.hosts = hosts

    def check_reload(self):
        cl = self.zmapdir + '/cluster_list'
        hl = self.zmapdir + '/host_list'
        cl_last = os.stat(cl).st_mtime
        hl_last = os.stat(hl).st_mtime
        if cl_last > self.cl_time:
            self.cl_time = cl_last
            self.load_clusters(cl)
        if hl_last > self.hl_time:
            self.hl_time = hl_last
            self.load_hosts(hl)

    def _lazy_delete_data(self):
        # Not thread-safe!
        keys = self.last_map.keys()
        for c in keys:
            # 1 hour till stale
            if time.time() - self.last_map[c] > 3600:
                del self.last_users[c]
                del self.last_map[c]

    ## finger ##

    @staticmethod
    def finger(ip, local=True):
        """ Get a list of users connected to ip. If local, get only users
            logged into a local tty (eg. :0, or tty7). """
        users = []
        try:
            # 1.5-second timeout
            sock = socket.create_connection((ip, 79), 1.50)
            sock.send('\n') # get all logged in users
            s = sock.recv(1024)
            if s.startswith('No one'):
                return users
            t = s.split('\n')
            v = t[0].index('Tty')
            for line in t[1:]:
                if not local or len(line) > v:
                    if line[v] == ':' or line[v:v + 4] == 'tty7':
                        users.append(line[:line.index(' ')])
        except (socket.error, socket.timeout):
            return users
        except:
            print "Error parsing socket response", sys.exc_info()[0]
        return users

    ## zmap helper commands ##

    def _cmap(self, cluster):
        usermap = {}
        for canon in self.clusters[cluster]:
            for (ip, host) in self.hosts[canon]:
                users = map(self._mapnick, self.finger(ip))
                for user in users:
                    usermap.setdefault(user, []).append(host)
        return usermap

    ## The zmap command ##

    @public_command(RegexCommand, zmapregex, "zmap",
                    pm_option=ALLOW, targeted=True)
    def cmap(self, src, chat, line, match):
        """ zmap <cluster>: List users that zmap sees in the cluster. """
        self.check_reload()
        cluster = match.group(1).lower()
        if cluster not in self.clusters:
            return nocluster % cluster
        self._lazy_delete_data()
        usermap = self._cmap(cluster)
        self.last_users[chat] = usermap
        self.last_map[chat] = time.time()
        if usermap:
            return clusterlings % (cluster, ', '.join(usermap.keys()))
        else:
            return noonethere % cluster

    # the 'what machines am i on' commands

    @public_command(RegexCommand, whereregex, "where",
                    pm_option=ALLOW, targeted=True)
    def where(self, src, chat, line, match):
        """ where is <nick> in <cluster>: Map the given cluster
            and return all machines with the user on them. """
        self.check_reload()
        nick, cluster = match.groups()
        cluster = cluster.lower()
        if cluster not in self.clusters:
            return nocluster % cluster
        self._lazy_delete_data()
        usermap = self._cmap(cluster)
        self.last_users[chat] = usermap
        self.last_map[chat] = time.time()
        ruser, user = self._getuser(src, nick)
        machines = usermap.get(user, [])
        if machines:
            return machinelings % (ruser, ', '.join(machines))
        else:
            return nomachines % (ruser, cluster)

    @public_command(RegexCommand, whichregex, "which",
                    pm_option=ALLOW, targeted=True)
    def which(self, src, chat, line, match):
        """ which [of those] [machines] (were|are) <nick|me>:
            List machines that the user was logged in on during the
            last zmap. Must be done in the same channel as that zmap. """
        self._lazy_delete_data()
        if chat not in self.last_map:
            return nomapping
        nick = match.group(1)
        ruser, user = self._getuser(src, nick)
        machines = self.last_users[chat].get(user)
        if machines:
            return pastmachinelings % (ruser, ', '.join(machines))
        else:
            return pastnomachines % ruser
            
