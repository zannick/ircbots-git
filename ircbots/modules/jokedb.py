# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""jokedb -- The Redis-backed joke database for jokeserver.

This Module provides jokeserver's interface with its joke library stored
in Redis. """

import json
import re
import random
import time
from irclib import nm_to_n
from threading import Thread
from redis import RedisError, ConnectionError
from command import CharTriggerCommand, RegexCommand, ALLOW, DISALLOW, intregex
from learndb import applyregex
from module import InstArg, ircbot_module, handles_event, admin_command, private_command, public_command
from mredis import Redis
from question import Question

version = "JokeDB 0.5 by bswolf"

## Backend strings

# get
librarysize = "I have %d jokes in my library."
activesize = "I have %d active jokes in my library."
untold = "I have %d untold jokes in my library."
nojoke = "I have no joke with the id %d."
whenadded = "Joke %d was added on %s."
whosejoke = "%s told me that joke."
whenlast = "I last told joke %d at %s."
timestold = "I've told joke %d a total of %d times."
jokelabels = "Joke %d has the following labels: %s."
jokerating = "Joke %d has a rating of %d."
numreactions = "Joke %d has %d reactions, of which %d are good and %d are bad."
noreaction = "I don't have a reaction numbered %d."
showreaction = "[%d/%d, %s] %s <%s> %s"
labelled = "I have %d jokes with the label '%s'."
whichlabelled = "The label '%s' occurs in jokes %s."
lasttold = "I last told a joke in %s at %s."
whichlast = "The last joke I told in %s was %d."
whichid = "That was joke %d."
bestjoke = "My highest rated joke is %d."
worstjoke = "My lowest rated joke is %d."
highrated = "My highest rated joke is %d points out of %d."
lowrated = "My lowest rated joke is %d points out of %d."
idontknow = "I don't seem to have that information."
nolast = "I haven't told any jokes to %s."
adminqueuesize = "There are %d joke(s) in the admin approve queue."

# edit
jokeadded = "Joke saved with id %d."
jokedisabled = "Disabled joke %d."
jokeenabled = "Enabled joke %d."
nosuchline = "Joke %d does not have a line %d."
jokeline = '%d.%d "%s"'
reactionset = "The reaction '%s' is now of type '%s'."

# Keys
numjokeskey = "numjokes"    # INT, number of jokes in the DB
numactivejokeskey = "numactivejokes"  # INT, number of enabled jokes in the DB
jokekey = "joke:%d"         # JSON string that is a list of lines in the joke
addedkey = "joke:%d:added"  # INT, UNIX time of when the joke was added
jokecontribkey = "joke:%d:contrib"    # STRING, user who submitted this joke
toldkey = "joke:%d:told"    # INT, number of times this joke has been told
lastkey = "joke:%d:last"    # INT, UNIX time of when the joke was last told
jokelabelkey = "joke:%d:labels" # SET of STRING labels for this joke
ratingkey = "joke:%d:rating"    # INT, number of positive reactions minus negative
recalckey = "joke:%d:recalc"    # Flag to indicate the rating should be recalculated.
reactionskey = "joke:%d:reactions"    # LIST of JSON strings of the form:
reactionsform = '{"user": "%s", "text": "%s", "time": %d, "type": %d}'
                # type is +/- 1 or 0.
disabledkey ="joke:%d:disabled" # INT, if nonzero, the joke is ignored and not told
labelkey = "jokes:%s"         # INT SET of jokes with the label
contribkey = "contrib:%s"     # INT SET of jokes the user has added
lastjokekey = "lastjoke:%s"   # INT, id of the last joke told in a channel
bestjokekey = "bestjoke"      # INT, id of the joke with highest rating
worstjokekey = "worstjoke"    # INT, id of the joke with lowest rating
newjokekey = "newjoke"        # INT, id of earliest untold joke
goodreactionskey = "reactions:good"       # SET of STRINGS
badreactionskey = "reactions:bad"         # SET of STRINGS
mehreactionskey = "reactions:meh"         # SET of STRINGS
unknownreactionskey = "reactions:unknown" # SET of STRINGS

unapprovedkey = "unapproved"  # STRING LIST, json encoded
                # {"joke": [text], "author": author, "added": added}
                # awaiting admin approval.
approvingkey = "approving:%s" # STRING LIST, admin-specific backup
                # for unapprovedkey

untoldkey = "untold"      # INT LIST of untold jokes

nextjokekey = "nextjoke"  # INT id of next joke to tell
nextchkey = "nextjoke:%s" # INT id of next joke to tell in a specific channel

# Errors
keyerror = "JokeDB: Key error in %s: %s"
unknownerror = "JokeDB: Unknown error in %s: %s"
badreactiontype = "Reaction type should be 'good', 'bad', 'meh', or 'unknown'."

dontunderstand = "I don't understand your query."
erroroccurred = "Something went wrong when I tried to do that."
nolastjoke = "I don't remember telling a joke to %s."

## Frontend strings

helplink = "You may wish to check out my manual, located at " + \
           "http://www.bswolf.com/jokeserver.html, " + \
           "or simply ask Zannick."

# joke telling

# joke recording

startedjoke = "I'm listening. " + \
              "Please note I prefer original jokes of 4 lines or less."
notmuch = "That wasn't much of a joke."
alreadyrecording = "You're already telling me a joke."
donerecord = "Okay, '%saddtemplabels' to add labels, '%ssubmit' to submit " + \
             "for approval, '%stesttempjoke' to test it, " + \
             "or '%sedittempjoke' to edit it."
stoprecordingfirst = "Stop recording with '%sendjoke' first."
listlabelsnew = "Your unsaved joke has labels: %s."

# tempjoke commands

nonewjoke = "You haven't told me a joke."
newjokenoline = "Your new joke has no line %d."

testline = 'new.%d "%s"'

# joke saving

jokesubmitted = "Joke submitted. Please allow time for consideration."
jokesubresult = "Your joke submitted on %s has been %sed."

stillapproving = "You are currently approving a joke. '%squeue get' to " + \
                 "see it again, or '%squeue pass' to return it to the queue."
notapproving = "You are not currently approving a joke. '%squeue size' " + \
               "to check the queue size, or '%squeue get' to get the " + \
               "next joke in the queue, if there is one."

jokeaccepted = "Accepted submission by %s as joke %d."
jokerejected = "Rejected submission by %s."

# joke lookup regexes 

jokeindex = re.compile(r"new|\d+", re.I)
jokeidregex = re.compile(r"that|last|\d+", re.I)
intregex = re.compile(intregex)

# Regexes for commands
contribregex = re.compile(
        r"who( i|')s responsible for ((joke )?\d+|that (last )?(joke|one))\??"
        r"|whose( joke)? (is|was) ((joke )?\d+|that( last)?( joke| one)?)\??"
        r"|who contributed ((joke )?\d+|that( last)?( joke| one)?)\??"
        r"|who told you ((joke )?\d+|that (last )?(joke|one))\??"
        r"|contributor ((joke )?\d+|last)\??", re.I)
jokesregex = re.compile(
        r"how many jokes( do you have| are)?( in your library)?\??"
        r"|how many jokes do you know\??"
        r"|num(ber)?( of)? jokes\??", re.I)
newjokesregex = re.compile(
        r"how many (new|untold) jokes( do you have| are)?( in your library)?\??"
        r"|num(ber)?( of)? (new|untold) jokes\??", re.I)
whenaddedregex = re.compile(
        r"when was ((joke )?\d+|that( last)?( joke| one)?) added\??"
        r"|when added ((joke )?\d+|that( last (joke|one))?)\??", re.I)
helpregex = re.compile(r"help\??", re.I)
jokeratingregex = re.compile(
        r"how( is|'s) ((joke )?\d+|that (last )?(joke|one)) rated\??"
        r"|what( is|'s) the rating of ((joke )?\d+|that (last )?(joke|one))\??"
        r"|what( is|'s) ((joke )?\d+|that (last )?(joke|one))'s rating\??", re.I)
requestjokeregex = re.compile(r"request(?: joke)? (\d+)", re.I)
bestjokeregex = re.compile(r"what( is|'s) your best( rated)? joke\??", re.I)
worstjokeregex = re.compile(r"what( is|'s) your worst( rated)? joke\??", re.I)
howdoiregex = re.compile(
        r"how do( i|es one) express approval/"
                "disapproval (of|for) (a|that) joke\??"
        r"|how do( i|es one) express (dis)?approval (of|for) (a|that) joke\??"
        r"|how do( i|es one) (up|down)mod (a joke|one of your jokes)\??", re.I)
howdoiregex2 = re.compile(r"how do( i|es one)", re.I)

@ircbot_module
class JokeDB(Redis):
    """ This class is used to manage a Redis-based jokedb. """
    def __init__(self, question, channels, privmsg, pubmsg, host='localhost',
                 port=6379, db=0, password=None, socket_timeout=None,
                 log=None, cmdchar='!', minwait=86401, maxwait=864001,
                 jokecontrol=60, jokefadetime=600):
        """ Creates a Redis client for the specified server.
            question - the bot's Question module.
            privmsg - the bot's privmsg method.
            pubmsg - the bot's pubmsg method.
            channels - the bot's channels index.
            log - an optional function to log important errors and failures.
                  It should take a single string as an argument.
            cmdchar - The command character for the commands this class
                      defines.
            minwait - How long before a joke can be told again.
            maxwait - Any joke that has been last told longer than this can
                      be chosen from all such jokes equally likely.
            jokecontrol - The min. length time between jokes told in a channel.
            jokefadetime - The max. length time to record reactions to a joke.
            """
        self.cmdchar = cmdchar # Done by Redis, but just to be sure
        super(JokeDB, self).__init__(host, port, db, password,
                                     socket_timeout, log, cmdchar)
        self.question = question
        self.channels = channels
        self.privmsg = privmsg
        self.pubmsg = pubmsg
        self.minwait = minwait
        self.maxwait = maxwait
        self.jokecontrol = jokecontrol
        self.jokefadetime = jokefadetime
        if self.redis.get(numjokeskey) is None:
            self.redis.set(numjokeskey, 0)
        if self.redis.get(numactivejokeskey) is None:
            self.redis.set(numactivejokeskey, 0)
        if self.redis.get(newjokekey) is None:
            self.redis.set(newjokekey, 0)
        self.tempjoke = {}
        self.recording = {}
        self.approving = {}
        self.config += ['approving', 'tempjoke']
        self.donerecord = donerecord % tuple([cmdchar] * 4)
        self.stoprecordingfirst = stoprecordingfirst % cmdchar
        self.stillapproving = stillapproving % (cmdchar, cmdchar)
        self.notapproving = notapproving % (cmdchar, cmdchar)

    ## Database get functions ##

    # Most of these return intended bot responses.
    # Some will have an argument 'raw' which can be set to True to
    # get the function to return only the raw value.

    def getjoke(self, id):
        """ Retrieves a joke and returns it as a list of strings.
            An invalid id will result in a return value of None. """
        try:
            s = self.redis.get(jokekey % id)
            if s:
                return json.loads(s)
        except Exception as e: # might be a json error
            self.log(unknownerror % ("getjoke", e))
            raise e
    
    def getnumjokes(self, raw=False):
        """ Retrieves the number of jokes. """
        try:
            n = int(self.redis.get(numjokeskey))
            if raw:
                return n
            return librarysize % n
        except RedisError as e:
            self.log(unknownerror % ("getnumjokes", e))
            raise e

    def getnumactivejokes(self, raw=False):
        """ Retrieves the number of jokes. """
        try:
            n = self.redis.get(numactivejokeskey)
            if raw:
                return n
            return activesize % n
        except RedisError as e:
            self.log(unknownerror % ("getnumactivejokes", e))
            raise e

    def getuntoldjokes(self, raw=False):
        """ Retrieves the number of untold jokes. """
        try:
            if raw:
                return self.redis.llen(untoldkey)
            return untold % self.redis.llen(untoldkey)
        except RedisError as e:
            self.log(unknownerror % ("getuntoldjokes", e))
            raise e
    
    def getwhenadded(self, id):
        """ Retrieves the UNIX time the specified joke was added. """
        try:
            s = self.redis.get(addedkey % id)
            if s:
                return whenadded % (id, time.ctime(int(s)))
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("getadded", e))
            raise e

    def getcontributor(self, id):
        """ Retrieves the nickname of the contributor of the joke. """
        try:
            s = self.redis.get(jokecontribkey % id)
            if s:
                return whosejoke % s
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("getcontributor", e))
            raise e
    
    def getjokesby(self, author):
        """ Retrieves the list of jokes submitted by a user. """
        try:
            jokes = sorted(self.redis.smembers(contribkey % author))
            return [int(i) for i in jokes]
        except RedisError as e:
            self.log(unknownerror % ("getjokesby", e))
            raise e

    def gettimestold(self, id):
        """ Retrieves the number of times the specified joke was told. """
        try:
            s = self.redis.get(toldkey % id)
            if s is not None:
                return timestold % (id, int(s))
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("gettimestold", e))
            raise e

    def getwhenlasttold(self, id, raw=False):
        """ Retrieves the last time the specified joke was told. """
        try:
            s = self.redis.get(lastkey % id)
            if s is not None:
                s = int(s)
                if raw:
                    return s
                return whenlast % (id, time.ctime(s))
            if raw:
                return None
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("getwhenlasttold", e))
            raise e

    def getlabels(self, id):
        """ Retrieves the labels of the specified joke. """
        try:
            llist = self.redis.smembers(jokelabelkey % id)
            if llist:
                return jokelabels % (id, ', '.join(sorted(llist)))
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("getlabels", e))
            raise e

    def getrating(self, id):
        """ Retrieves the rating of the specified joke. If a reaction
            has been updated, this will quickly recalculate the rating
            before returning it. """
        try:
            rc = self.redis.get(recalckey % id)
            if rc is not None:
                # can only happen if id is valid
                if int(rc):
                    self._recalc(int(id))
                r = int(self.redis.get(ratingkey % id))
                if r:
                    return jokerating % (id, r)
            return nojoke % id
        except RedisError as e:
            self.log(unknownerror % ("getrating", e))
            raise e

    def _recalc(self, id):
        """ An internal function that recalculates the rating of a joke
            based on the reactions. """
        reactions = map(json.loads, self.redis.lrange(reactionskey % id, 0, -1) or [])
        unknown = 0
        # important to count only the most recent of any user's reaction
        user = {}
        for i in range(len(reactions)):
            r = reactions[i]
            rtype = self.getreactiontype(r["text"])
            if rtype == 'good':
                r["type"] = 1
                user[r["user"]] = 1
            elif rtype == 'bad':
                r["type"] = -1
                user[r["user"]] = -1
            elif rtype == 'meh':
                r["type"] = 0
                # meh votes don't overwrite old votes
            else:
                r["type"] = 0
                unknown = 1
                # unknown votes don't overwrite old votes
            self.redis.lset(reactionskey % id, i, json.dumps(r))
        self.redis.set(ratingkey % id, sum(user.values()))
        if unknown:
            self.redis.set(recalckey % id, 1)
        else:
            self.redis.set(recalckey % id, 0)

    def getnumreactions(self, id):
        """ Retrieves the number and types of reactions to the specified joke. """
        try:
            r = self.redis.lrange(reactionskey % id, 0, -1)
            if r is None:
                return nojoke % id
            reactions = map(json.loads, r)
            good = 0
            bad = 0
            total = len(reactions)
            for r in reactions:
                if r["type"] > 0:
                    good += 1
                elif r["type"] < 0:
                    bad += 1
            return numreactions % (id, total, good, bad)
        except RedisError as e:
            self.log(unknownerror % ("getnumreactions", e))
            raise e

    def getreaction(self, id, index):
        """ Retrieves a particular reaction to the specified joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            maxr = self.redis.llen(reactionskey % id)
            if index <= 0 or index > maxr:
                return noreaction % index
            r = json.loads(self.redis.lindex(reactionskey % id, index-1))
            val = self.getreactiontype(r["text"])
            return showreaction % (index, maxr, val, time.ctime(r["time"]),
                               r["user"], r["text"])
        except KeyError as e:
            self.log(keyerror % ("getreaction", e))
            raise e
        except RedisError as e:
            self.log(unknownerror % ("getreaction", e))
            raise e

    def isdisabled(self, id):
        """ Retrieves whether the specified joke has been disabled. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            return (int(self.redis.get(disabledkey % id)) != 0)
        except RedisError as e:
            self.log(unknownerror % ("isdisabled", e))
            raise e

    def getjokeswithlabel(self, label, raw=False):
        """ Retrieves all jokes with the specified label. """
        try:
            lj = sorted(self.redis.smembers(labelkey % label))
            if raw:
                return [int(i) for i in lj]
            return whichlabelled % (label,', '.join(lj))
        except RedisError as e:
            self.log(unknownerror % ("getjokeswithlabel", e))
            raise e
    
    def getnumjokeswithlabel(self, label):
        """ Retrieves the number of jokes with the specified label. """
        try:
            return labelled % (self.redis.scard(labelkey % label), label)
        except RedisError as e:
            self.log(unknownerror % ("getnumjokeswithlabel", e))
            raise e

    def getlastjoke(self, channel, raw=False):
        """ Retrieves the id of the last joke told in a given channel. """
        channel = channel.lower()
        try:
            id = self.redis.get(lastjokekey % channel)
            if id is not None:
                id = int(id)
                if raw:
                    return int(id)
                return whichlast % int(id)
            if not raw:
                return nolast % channel
        except RedisError as e:
            self.log(unknownerror % ("getlastjoke", e))
            raise e

    def getwhenlastjoke(self, channel, raw=False):
        """ Retrieves the time the last joke was told in a given channel. """
        channel = channel.lower()
        try:
            id = self.redis.get(lastjokekey % channel)
            if id is not None:
                id = int(id)
                if raw:
                    return int(self.redis.get(lastkey % id))
                return lasttold % time.ctime(self.redis.get(lastkey % id))
            if raw:
                return 0
            return nolast % channel
        except RedisError as e:
            self.log(unknownerror % ("getwhenlastjoke", e))
            raise e

    def getbestjoke(self, raw=False):
        """ Retrieves the id of the joke with highest rating. """
        try:
            self.findbestworstjoke(5.0)
            id = self.redis.get(bestjokekey)
            if id is not None:
                id = int(id)
                if raw:
                    return id
                return bestjoke % id
            if raw:
                return None
            return idontknow
        except RedisError as e:
            self.log(unknownerror % ("getbestjoke", e))
            raise e

    def getworstjoke(self, raw=False):
        """ Retrieves the id of the joke with lowest rating. """
        try:
            self.findbestworstjoke(5.0)
            id = self.redis.get(worstjokekey)
            if id is not None:
                id = int(id)
                if raw:
                    return id
                return worstjoke % id
            if raw:
                return None
            return idontknow
        except RedisError as e:
            self.log(unknownerror % ("getworstjoke", e))
            raise e

    def getbestrating(self, raw=False):
        """ Retrieves the rating of the best joke. """
        try:
            self.findbestworstjoke(5.0)
            id = self.redis.get(bestjokekey)
            if id is None:
                if raw:
                    return None
                return idontknow
            id = int(id)
            rating = self.redis.get(ratingkey % id)
            if raw:
                return rating
            r = self.redis.llen(reactionskey % id)
            return highrated % (rating, r)
        except RedisError as e:
            self.log(unknownerror % ("getbestrating", e))
            raise e

    def getworstrating(self, raw=False):
        """ Retrieves the rating of the worst joke. """
        try:
            self.findbestworstjoke(5.0)
            id = self.redis.get(worstjokekey)
            if id is None:
                if raw:
                    return None
                return idontknow
            id = int(id)
            rating = self.redis.get(ratingkey % id)
            if raw:
                return rating
            r = self.redis.llen(reactionskey % id)
            return lowrated % (rating, r)
        except RedisError as e:
            self.log(unknownerror % ("getworstrating", e))
            raise e

    def getreactiontype(self, reaction):
        """ Indicates whether the reaction is known to be
            good, bad, or meh. """
        lreact = reaction.lower()
        try:
            if self.redis.sismember(goodreactionskey, lreact):
                return 'good'
            elif self.redis.sismember(badreactionskey, lreact):
                return 'bad'
            elif self.redis.sismember(mehreactionskey, lreact):
                return 'meh'
            else:
                self.redis.sadd(unknownreactionskey, lreact)
                return 'unknown'
        except RedisError as e:
            self.log(unknownerror % ("getreactiontype", e))
            raise e

    #### SET functions are all admin-only ####

    def add(self, contributor, tokens, labels=(), raw=False):
        """ Adds a new joke, given the tokens (a list of strings)
            and optional set of labels for the joke. """
        try:
            id = self.redis.incr(numjokeskey) - 1 # atomic incr gives unique id
            # Leave this out of the pipeline so an error will stop the add here
            self.redis.mset({
                    jokekey % id        : json.dumps(tokens),
                    addedkey % id       : int(time.time()),
                    jokecontribkey % id : contributor,
                    toldkey % id        : 0,
                    lastkey % id        : 0,
                    ratingkey % id      : 0,
                    recalckey % id      : 0,
                    # Approved
                    disabledkey % id    : 0
                })
            p = self.redis.pipeline(transaction=True) # Make it atomic!
            # Untold
            p.rpush(untoldkey, id)
            p.incr(numactivejokeskey)
            for label in labels:
                p.sadd(jokelabelkey % id, label)
                p.sadd(labelkey % label, id)
            p.sadd(contribkey % contributor, id)
            d = p.execute()
            for a in d:
                if isinstance(a, Exception):
                    self.log(unknownerror % ("add_atom", a))
            if raw:
                return id
            return jokeadded % id
        # We DO NOT retry here as we might have failed in the middle
        # Retrying could make the problem more serious
        except RedisError as e:
            self.log(unknownerror % ("add", e))
            raise e

    def addlabels(self, id, labels):
        """ Adds multiple labels to the specified joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            for label in labels:
                self.redis.sadd(jokelabelkey % id, label)
                self.redis.sadd(labelkey % label, id)
            labels = self.redis.smembers(jokelabelkey % id)
            return jokelabels % (id, ', '.join(sorted(labels)))
        except RedisError as e:
            self.log(unknownerror % ("addlabels", e))
            raise e

    def removelabels(self, id, labels):
        """ Removes the specified labels from the specified joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            for label in labels:
                if self.redis.srem(jokelabelkey % id, label):
                    self.redis.srem(labelkey % label, id)
            labels = self.redis.smembers(jokelabelkey % id)
            return jokelabels % (id, ', '.join(sorted(labels)))
        except RedisError as e:
            self.log(unknownerror % ("removelabel", e))
            raise e

    def disablejoke(self, id):
        """ Disables the specified joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            d = int(self.redis.getset(disabledkey % id, 1))
            if d == 0:
                self.redis.decr(numactivejokeskey)
            return jokedisabled % id
        except RedisError as e:
            self.log(unknownerror % ("disablejoke", e))
            raise e

    def enablejoke(self, id):
        """ Enables the specified joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            d = int(self.redis.getset(disabledkey % id, 0))
            if d:
                self.redis.incr(numactivejokeskey)
            return jokeenabled % id
        except RedisError as e:
            self.log(unknownerror % ("enablejoke", e))
            raise e

    def editline(self, id, line, regex):
        """ Edit a line in the joke by using a vim-style regex,
            s/FIND/REPLACE/, where the line is matched against the FIND
            regex. 'g' and/or 'i' can be added to the end of the regex. """
        try:
            s = self.redis.get(jokekey % id)
            if not s:
                return nojoke % id
            tokens = json.loads(self.redis.get(jokekey % id))
            if line < 1 or line > len(tokens):
                return nosuchline % (id, line)
            #(old, delay) = tokens[line - 1]
            old = tokens[line - 1]
            (success, result, numrep) = applyregex(old, regex)
            if not success: # Error
                return result
            #tokens[line - 1] = (result, delay)
            tokens[line - 1] = result
            self.redis.set(jokekey % id, json.dumps(tokens))
            return jokeline % (id, line, result)
        except RedisError as e:
            self.log(unknownerror % ("editline", e))
            raise e

    def setreactiontype(self, reaction, rtype):
        """ Change or set a reaction as a particular type,
            either 'good', 'bad', or 'meh'. Anything else will cause
            the reaction to become unknown. """
        lreact = reaction.lower()
        try:
            p = self.redis.pipeline(transaction=True)
            if rtype == 'good':
                p.sadd(goodreactionskey, lreact)
                p.srem(badreactionskey, lreact)
                p.srem(mehreactionskey, lreact)
                p.srem(unknownreactionskey, lreact)
            elif rtype == 'bad':
                p.srem(goodreactionskey, lreact)
                p.sadd(badreactionskey, lreact)
                p.srem(mehreactionskey, lreact)
                p.srem(unknownreactionskey, lreact)
            elif rtype == 'meh':
                p.srem(goodreactionskey, lreact)
                p.srem(badreactionskey, lreact)
                p.sadd(mehreactionskey, lreact)
                p.srem(unknownreactionskey, lreact)
            else:
                p.srem(goodreactionskey, lreact)
                p.srem(badreactionskey, lreact)
                p.srem(mehreactionskey, lreact)
                p.sadd(unknownreactionskey, lreact)
                rtype = 'unknown'
            p.execute()
            # Recalculation may be necessary
            numjokes = int(self.redis.get(numjokeskey))
            for i in range(numjokes):
                self.redis.set(recalckey % i, 1)
            return reactionset % (reaction, rtype)
        except RedisError as e:
            self.log(unknownerror % ("setreactiontype", e))
            raise e
    
    def addreaction(self, id, text, user, time):
        """ Add a reaction to a joke. """
        try:
            max = int(self.redis.get(numjokeskey))
            if id < 0 or id >= max:
                return nojoke % id
            rtype = self.getreactiontype(text)
            if rtype == 'good':
                r = 1
            elif rtype == 'bad':
                r = -1
            else:
                r = 0
            self.redis.set(recalckey % id, 1)
            self.redis.rpush(reactionskey % id,
                             reactionsform % (user, text, time, r))
        except RedisError as e:
            self.log(unknownerror % ("addreaction", e))
            raise e

    #### Admin Joke Approval Functions ####

    # Push to the unapproved list
    def addtoapprovequeue(self, author, tokens, labels=None):
        try:
            joke = {"joke": tokens, "author": author,
                    "labels": list(labels) or [], "added": int(time.time())}
            self.redis.lpush(unapprovedkey, json.dumps(joke))
            return True
        except RedisError as e:
            self.log(unknownerror % ("addtoapprovequeue", e))
            raise e

    def approvequeuesize(self):
        try:
            return adminqueuesize % self.redis.llen(unapprovedkey)
        except RedisError as e:
            self.log(unknownerror % ("approvequeuesize", e))
            raise e

    def checkapproving(self, approver):
        # To prevent someone from locking down the approve queue
        try:
            return self.redis.llen(approvingkey % approver) >= 1
        except RedisError as e:
            self.log(unknownerror % ("checkapproving", e))
            raise e

    def getjokeforapproval(self, approver):
        # raw mode only
        try:
            asize = self.redis.llen(approvingkey % approver)
            # Always make sure that the first in the approvingkey queue
            # is the joke they're approving currently
            if asize > 0:
                joke = self.redis.lindex(approvingkey % approver, -1)
            else:
                queuesize = self.redis.llen(unapprovedkey)
                if queuesize == 0:
                    return None
                joke = self.redis.rpoplpush(unapprovedkey, approvingkey % approver)
            return json.loads(joke)
        except RedisError as e:
            self.log(unknownerror % ("getjokeforapproval", e))
            raise e

    def approveresult(self, approver, result):
        try:
            if result == "accept":
                joke = json.loads(self.redis.rpop(approvingkey % approver))
                tokens = joke["joke"]
                author = joke["author"]
                labels = joke["labels"]
                added = joke["added"]
                id = self.add(author, tokens, labels, raw=True)
                self.redis.set(addedkey % id, added)
                return jokeaccepted % (author, id)
            elif result == "reject":
                joke = json.loads(self.redis.rpop(approvingkey % approver))
                author = joke["author"]
                return jokerejected % author
            else:
                # "pass"
                self.redis.rpoplpush(approvingkey % approver, unapprovedkey)
                return "Passing on that joke..."
        except RedisError as e:
            self.log(unknownerror % ("approveresult", e))
            raise e

    #### Keeping track of things ####

    def toldjoke(self, id, channel):
        try:
            t = self.redis.incr(toldkey % id)
            if t == 1:
                self.redis.lrem(untoldkey, id)
            self.redis.mset({ lastkey % id          : int(time.time()),
                              lastjokekey % channel : id })
            rch = self.redis.get(nextchkey % channel)
            if rch is not None and int(rch) == id:
                self.redis.delete(nextchkey % channel)
            else:
                self.redis.delete(nextjokekey)
        except RedisError as e:
            self.log(unknownerror % ("toldjoke", e))
            raise e

    def _findbestworstjoke(self):
        try:
            numjokes = int(self.redis.get(numjokeskey))
            best = (0, -100)
            worst = (0, 100)
            for jokeid in range(numjokes):
                if int(self.redis.get(recalckey % jokeid)):
                    self._recalc(jokeid)
                r = int(self.redis.get(ratingkey % jokeid))
                if r > best[1]:
                    best = (jokeid, r)
                if r < worst[1]:
                    worst = (jokeid, r)
            self.redis.mset({ bestjokekey : best[0],
                              worstjokekey: worst[0] })
        except RedisError as e:
            self.log(unknownerror % ("_findbestworstjoke", e))
            raise e

    def findbestworstjoke(self, wait=0.0):
        t = Thread(target=self._findbestworstjoke, args=())
        t.start()
        if wait:
            t.join(wait)
            return not(t.isAlive())
        return None

    #### Select a joke to tell! ####

    def __selectjoke(self, raw=False):
        try:
            if self.getuntoldjokes(raw=True) > 0:
                self.redis.set(nextjokekey, self.redis.lindex(untoldkey, 0))
            else:
                max = int(self.redis.get(numjokeskey))
                now = time.time()
                def exclude(id):
                    if int(self.redis.get(disabledkey % id)):
                        return False
                    t = int(self.redis.get(lastkey % id))
                    if now - t < self.minwait:
                        return False
                    return True
                jlist = filter(exclude, range(max))
                if jlist:
                    j = random.choice(jlist)
                    if raw:
                        return j
                    self.redis.set(nextjokekey, j)
                else:
                    self.log("Unable to select a joke.")
        except RedisError as e:
            self.log(unknownerror % ("__selectjoke", e))
            raise e
        except Exception as e:
            self.log("A terrifying error occurred: " + e)
            raise e

    def _scorejokes(self, jokeids):
        scores = {}
        now = time.time()
        for id in jokeids:
            if int(self.redis.get(disabledkey % id)):
                continue
            t = int(self.redis.get(lastkey % id))
            if int(self.redis.get(recalckey % id)):
                self._recalc(id)
            r = int(self.redis.get(ratingkey % id))
            n = int(self.redis.get(toldkey % id))
            #nr = self.redis.llen(reactionskey % id)
            if now - t < self.minwait:
                # Never tell a joke this soon again
                continue
            elif now - t < self.maxwait:
                scores[id] = 1 + r + \
                    10 * (t - self.minwait) / (self.maxwait - self.minwait)
            else:
                # instead of r^2, this preserves sign.
                scores[id] = r * abs(r) + 10
            # Penalize jokes told too much.
            scores[id] -= n / 10
        return scores

    @staticmethod
    def _weightedrandom(s):
        # s is a list of pairs, (a, b) where b is the weight
        # for choosing a randomly.
        s2 = []
        # s2 is a similar list, except the b in each (a,b) pair
        # is accumulative, so that if a random value falls between
        # two consecutive b's, then the latter a was chosen.
        v = 0
        for a, b in s:
            v += b
            s2 += [(a,v)]
        r = random.uniform(0, v)
        j = s[-1][0] # if our random value never exceeds any b, use the last a
        for a, b in s:
            if r < b:
                j = a
                break
        return j

    def _selectjoke(self, raw=False):
        try:
            if self.getuntoldjokes(raw=True) > 0:
                self.redis.set(nextjokekey, self.redis.lindex(untoldkey, 0))
            else:
                max = int(self.redis.get(numjokeskey))
                scores = self._scorejokes(range(max))
                s = sorted(scores.items(), key=lambda (a, b): b, reverse=True)
                if s:
                    j = self._weightedrandom(s)
                    if raw:
                        return j
                    self.redis.set(nextjokekey, j)
                else:
                    self.log("Unable to select a joke.")
        except RedisError as e:
            self.log(unknownerror % ("_selectjoke", e))
            raise e
        except Exception as e:
            self.log("A terrifying error occurred: " + str(e))
            raise e
    
    # Select a joke asynchronously, but optionally waits a while
    def selectjoke(self, wait=0.0):
        # Disabled self._selectjoke as it tends to bias.
        selector = self.__selectjoke
        t = Thread(target=selector, args=())
        t.start()
        if wait > 0.0:
            t.join(wait)
            return not(t.isAlive())
        return None

    def selectjokefromset(self, jokes):
        scores = self._scorejokes(jokes)
        s = sorted(scores.items(), key=lambda (a, b): b, reverse=True)
        if s:
            def rc(s):
                return random.choice(s)[0]
            selector = random.choice([self._weightedrandom, rc])
            j = selector(s)
            return j
        return None

    def selectjokewithtags(self, tags):
        jokesbytag = [set(self.getjokeswithlabel(x, raw=True)) for x in tags]
        allmatched = reduce(lambda a, b: a & b, jokesbytag)
        return self.selectjokefromset(allmatched)

    def selectjokebyauthor(self, author):
        return self.selectjokefromset(self.getjokesby(author))

    def getnextjoke(self, channel):
        try:
            j = self.redis.get(nextchkey % channel.lower())
            if j is not None:
                return int(j)
            k = self.redis.get(nextjokekey)
            if k is not None:
                return int(k)
        except RedisError as e:
            self.log(unknownerror % ("getnextjoke", e))
            raise e

    ## Frontend

    def _extractid(self, line, channel):
        """ Search for a joke id or the word "last", in which case a lookup
            is performed for the last joke told in a channel. """
        idm = jokeidregex.search(line)
        if idm:
            id = idm.group(0)
            if id in ["that", "last"]:
                return self.getlastjoke(channel, raw=True)
            else:
                return int(id)
        return -1

    def _handleid(self, chat, line, func):
        """ Extract a joke id from the given line, and if successful,
            pass it to func. """
        id = self._extractid(line, chat)
        if id is not None:
            if id >= 0:
                if id < self.getnumjokes(raw=True):
                    return func(id)
                return nojoke % id
            return dontunderstand
        return nolastjoke % chat

    ## Commands ##

    @public_command(RegexCommand, contribregex, "/joke_contrib",
                    pm_option=ALLOW, targeted=True)
    def contrib(self, src, chat, line, match):
        """ Look up who told a joke to the bot. """
        return self._handleid(chat, line, self.getcontributor)

    @public_command(RegexCommand, jokesregex, "/jokes_total",
                    pm_option=ALLOW, targeted=True)
    @public_command(CharTriggerCommand, InstArg("cmdchar"), "jokes", [],
                    pm_option=ALLOW, targeted=False)
    def numjokes(self, *args):
        # *args allows Char and Regex commands without any real args
        """ jokes: Retrieve the number of jokes in the library. """
        return self.getnumjokes()

    @public_command(RegexCommand, newjokesregex, "/jokes_new",
                    pm_option=ALLOW, targeted=True)
    def untoldjokes(self, *args):
        """ Retrieve the number of untold (new) jokes in the library. """
        return self.getuntoldjokes()

    @public_command(RegexCommand, whenaddedregex, "/joke_whenadded",
                    pm_option=ALLOW, targeted=True)
    def whenadded(self, src, chat, line, match):
        """ Look up when a joke was added to the bot. """
        return self._handleid(chat, line, self.getwhenadded)

    @public_command(RegexCommand, helpregex, "/joke_help",
                    pm_option=ALLOW, targeted=True)
    def help(self, *args):
        """ Return a help string. """
        return ("%sjoke for a joke. " % self.cmdchar) + helplink

    @public_command(RegexCommand, howdoiregex2, "/joke_howdoi2",
                    pm_option=ALLOW, targeted=True)
    def howdoi(self, *args):
        """ Return a more different help string. """
        return helplink

    @public_command(RegexCommand, jokeratingregex, "/joke_rating",
                    pm_option=ALLOW, targeted=True)
    def rating(self, src, chat, line, match):
        """ Look up the rating of a joke. """
        return self._handleid(chat, line, self.getrating)

    @public_command(RegexCommand, requestjokeregex, "/joke_request",
                    pm_option=DISALLOW, targeted=True)
    def request(self, src, chat, line, match):
        """ Request that a particular told be told next. """
        id = int(match.group(1))
        if id >= 0 and id < self.getnumjokes(raw=True):
            try:
                self.redis.set(nextchkey % chat.lower(), id)
                return "Okay, I'll tell that joke next."
            except RedisError as e:
                self.log(unknownerror % ("request", e))
                return erroroccurred
        return nojoke % id

    @public_command(RegexCommand, bestjokeregex, "/joke_bestjoke",
                    pm_option=ALLOW, targeted=True)
    def bestjoke(self, *args):
        """ Look up which joke is rated highest. """
        return self.getbestjoke()

    @public_command(RegexCommand, worstjokeregex, "/joke_worstjoke",
                    pm_option=ALLOW, targeted=True)
    def worstjoke(self, *args):
        """ Look up which joke is rated lowest. """
        return self.getworstjoke()

    ## Joke reactions

    @handles_event("pubmsg", -10)
    def _reaction(self, connection, event):
        """ Check whether this is a reaction to a joke recently told. """
        src = event.source()
        if src:
            src = nm_to_n(src)
        chat = event.target()
        msg = event.arguments()[0].lower().strip()
        nick = connection.get_nickname().lower()
        if (msg.startswith(nick) and len(msg) > len(nick)
            and msg[len(nick)] in ':,'):
            msg = msg[len(nick) + 1:].strip()
        elif nick in msg:
            # check if it's an upmod or downmod
            if "++" + nick in msg or nick + "++" in msg:
                if self.getreactiontype(msg) != "good":
                    self.setreactiontype(msg, "good")
            elif "--" + nick in msg or nick + "--" in msg:
                if self.getreactiontype(msg) != "bad":
                    self.setreactiontype(msg, "bad")
            else:
                # has our nick in it but isn't a response!
                return None
        else:
            # Doesn't include our name anywhere!
            return None
        # add it to the reactions of the most recent joke in this channel
        id = self.getlastjoke(chat, raw=True)
        if id is not None:
            t = time.time()
            last = self.getwhenlasttold(id, raw=True)
            if t - last < self.jokefadetime:
                rtype = self.getreactiontype(msg)
                # don't record unknown reactions for space reasons
                if rtype in ["good", "bad", "meh"]:
                    self.addreaction(id, msg, src, t)

    ## Joke submissions

    # Logs at -15, commands at -10. At -11 we can log and not hit commands
    @handles_event("privmsg", -11)
    @handles_event("privnotice", -11)
    def _recording(self, connection, event):
        """ Checks if we are recording this user, and records if so. """
        src = event.source()
        if src:
            src = nm_to_n(src)
        if src in self.recording:
            msg = event.arguments()[0]
            if not msg.startswith(self.cmdchar): # allow eg. !endjoke
                self.tempjoke[src]["joke"] += [msg.strip()]
                return "NO MORE" # prevent commands from being parsed

    def _startrecording(self, nick):
        self.tempjoke[nick] = { "joke": [], "labels": set(),
                                "timestamp": time.time() }
        self.recording[nick] = True

    @private_command(CharTriggerCommand, InstArg("cmdchar"), "startjoke", [])
    def startrecording(self, src, *args):
        """ startjoke: Starts recording your joke. The bot is recording your
            joke if it replies "I'm listening." to this command. """
        if src in self.recording:
            return alreadyrecording
        if src in self.tempjoke:
            def nuke(str):
                if str.lower() == 'yes':
                    self._startrecording(src)
                    return (True, "Okay, then. " + startedjoke)
                else:
                    return (True, self.donerecord)
            self.question.registerquestion(src, src, nuke)
            return "You currently have an unsaved joke. Overwrite it? (yes/no)"
        self._startrecording(src)
        return startedjoke

    @private_command(CharTriggerCommand, InstArg("cmdchar"), "endjoke", [])
    def endrecording(self, src, *args):
        """ endjoke: Stops recording your joke. """
        if src in self.recording:
            del self.recording[src]
            if len(self.tempjoke[src]["joke"]) == 0:
                del self.tempjoke[src]
                return notmuch
            else:
                return self.donerecord
        else:
            return "You're not telling a joke."

    ## Joke reviewing

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "queue", [str])
    def _queue(self, src, chat, command):
        """ queue <command>: Perform various tasks associated with the joke
            approval queue. Subcommands include: size, check, get, accept,
            reject, pass. """
        command = command.lower()
        if command == "size":
            return self.approvequeuesize()
        elif command == "check":
            if self.checkapproving(src):
                return self.stillapproving
            else:
                return self.notapproving
        elif command == "get":
            joke = self.getjokeforapproval(src)
            if joke is None:
                return "The approve queue is empty."
            else:
                # tell joke to src
                # Make this asynchronous?
                self._telljoke(src, joke["joke"])
                return None
        elif command in ["accept", "reject", "pass"]:
            if self.checkapproving(src):
                joke = self.getjokeforapproval(src)
                author = joke["author"]
                subtime = time.ctime(joke["added"])
                res = self.approveresult(src, command)
                if command == "pass":
                    return res
                # Message user to inform them of result, if they're
                # in a channel the bot is in
                for chan in self.channels:
                    if author in self.channels[chan].users():
                        msg = jokesubresult % (subtime, command)
                        self.privmsg(author, msg)
                        break
                return res
            else:
                return "You don't have a joke to approve."
        else:
            return "'%s' is not a valid queue command." % command

    ## Joke telling

    def _telljoke(self, target, tokens):
        for line in tokens:
            t = min(random.gauss(2.718281828, 0.75), 5.0)
            time.sleep(t)
            if target[0] == '#':
                self.pubmsg(target, line)
            else:
                self.privmsg(target, line)

    @public_command(CharTriggerCommand, InstArg("cmdchar"), "joke", [list],
                    pm_option=DISALLOW, targeted=False)
    def joke(self, src, chat, tags):
        """ joke: Tells a joke. May be subject to harsh ratelimiting. """
        lastid = self.getlastjoke(chat, raw=True)
        last = None
        if lastid:
            last = self.getwhenlasttold(lastid, raw=True)
        if last:
            if time.time() < last + self.jokecontrol:
                return "I just told a joke! Be patient."
        print "determining joke from tags:", tags
        if len(tags) == 0:
            nextid = self.getnextjoke(chat)
            if nextid is None:
                if self.selectjoke(10.0):
                    nextid = self.getnextjoke(chat)
                    if nextid is None:
                        return "I don't seem to have any jokes available."
                else:
                    return "I'm thinking, ask me again in a few minutes."
        elif len(tags) == 1:
            if intregex.match(tags[0]):
                nextid = int(tags[0])
                if nextid < 0 or nextid >= self.getnumjokes(raw=True):
                    return nojoke % nextid
            else:
                # may be author
                nextid = self.selectjokebyauthor(tags[0])
                if nextid is None:
                    nextid = self.selectjokewithtags(tags)
                    if nextid is None:
                        return "I can't find a joke with that tag."
        else:
            nextid = self.selectjokewithtags(tags)
            if nextid is None:
                return "I can't find a joke with those tags."
        # This is in the main thread; hence the bot will not see or handle
        # any events here until it is done telling the joke.
        print "chose", nextid
        self._telljoke(chat, self.getjoke(nextid))
        self.toldjoke(nextid, chat)

    @private_command(CharTriggerCommand, InstArg("cmdchar"),
                     "testtempjoke", [])
    def testtempjoke(self, src, chat):
        """ testtempjoke: Tests your unsaved joke by telling it to you. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        t = Thread(target=self._telljoke,
                   args=(src, self.tempjoke[src]["joke"]))
        t.start()

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "testjoke", [int])
    def testjoke(self, src, chat, jokeid):
        """ testjoke <jokeid>: Tests a joke by telling it to you. """
        t = Thread(target=self._telljoke, args=(src, self.getjoke(jokeid)))
        t.start()

    ## All other commands

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "addlabels",
                   [int, list])
    def _addlabels(self, src, chat, jokeid, labels):
        """ addlabels <jokeid> <list of labels>: Add the listed labels to
            the specified joke. """
        return self.addlabels(jokeid, labels)

    @private_command(CharTriggerCommand, InstArg("cmdchar"), "addtemplabels",
                     [list])
    def addtemplabels(self, src, chat, labels):
        """ addtemplabels <list of labels>: Add the listed labels to
            your unsaved joke. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        sl = self.tempjoke[src]["labels"]
        sl |= set(labels)
        self.tempjoke[src]["labels"] = sl
        return listlabelsnew % ', '.join(sl)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "disablejoke",
                   [int])
    def _disablejoke(self, src, chat, jokeid):
        """ disablejoke <jokeid>: Disable the specified joke. """
        return self.disablejoke(jokeid)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "enablejoke", [int])
    def _enablejoke(self, src, chat, jokeid):
        """ enablejoke <jokeid>: Enable the specified joke
            (if it's been disabled). """
        return self.enablejoke(jokeid)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "editjoke",
                   [int, int, str])
    def _editjoke(self, src, chat, jokeid, line, regex):
        """ editjoke <jokeid> <line> s/replace-this/with-this/[g][i]:
            Edit a line in a joke by applying a regex to it.
            You can add g and/or i at the end to replace all occurrences
            and/or ignore case, respectively. """
        return self.editline(jokeid, line, regex)

    @private_command(CharTriggerCommand, InstArg("cmdchar"), "edittempjoke",
                     [int, str])
    def edittempjoke(self, src, chat, line, regex):
        """ editjoke <line> s/replace-this/with-this/[g][i]:
            Edit a line in your unsaved joke by applying a regex to it.
            You can add g and/or i at the end to replace all occurrences
            and/or ignore case, respectively. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        if line < 1 or line > len(self.tempjoke[src]["joke"]):
            return newjokenoline % line
        old = self.tempjoke[src]["joke"][line - 1]
        success, result, numrep = applyregex(old, regex)
        if not success:
            return result
        self.tempjoke[src]["joke"][line - 1] = result
        return testline % (line, result)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "numreactions",
                   [int])
    def _numreactions(self, src, chat, jokeid):
        """ numreactions <jokeid>: Gives the number of recorded reactions to
            a joke, and indicates how many are of each type. """
        return self.getnumreactions(jokeid)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "getreaction",
                   [int, int])
    def _getreaction(self, src, chat, jokeid, index):
        """ getreaction <jokeid> <index>: Retrieves a specific reaction
            to a joke. """
        return self.getreaction(jokeid, index)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "recalc", [int])
    def recalc(self, src, chat, jokeid):
        """ recalc <jokeid>: Forces a recalculation of a joke's rating. """
        self.redis.set(recalckey % jokeid, 1)
        return "Okay, I will recalculate that next time it's requested."

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "release", [])
    def release(self, src, chat):
        """ release: Saves your joke directly to the library. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        res = self.add(src, self.tempjoke[src]["joke"],
                       self.tempjoke[src]["labels"])
        del self.tempjoke[src]
        return res

    @private_command(CharTriggerCommand, InstArg("cmdchar"), "submit", [])
    def submit(self, src, chat):
        """ submit: Saves your joke. An admin must approve it before it is
            put into the database. You will not be able to edit your joke
            after you submit it. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        self.addtoapprovequeue(src, self.tempjoke[src]["joke"],
                               self.tempjoke[src]["labels"])
        del self.tempjoke[src]
        return jokesubmitted

    @private_command(CharTriggerCommand, InstArg("cmdchar"),
                     "removetemplabels", [list])
    def removetemplabels(self, src, chat, labels):
        """ removetemplabels <list of labels>: Remove the listed labels
            from your unsaved joke. """
        if src not in self.tempjoke:
            return nonewjoke
        if src in self.recording:
            return self.stoprecordingfirst
        sl = self.tempjoke[src]["labels"]
        sl -= set(labels)
        self.tempjoke[src]["labels"] = sl
        return listlabelsnew % ', '.join(sl)
    
    @admin_command(CharTriggerCommand, InstArg("cmdchar"),
                   "removelabels", [int, list])
    def _removelabels(self, src, chat, jokeid, labels):
        """ removelabels <jokeid> <list of labels>: Remove the listed labels
            from a joke. """
        return self.removelabels(jokeid, labels)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "selectjoke", [])
    def _select(self, src, chat):
        """ select: Starts a thread running the selectjoke function.
            The result of this function call is not verified. """
        self.selectjoke()
        return "Okay."

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "setreaction",
                   [str, str])
    def _setreaction(self, src, chat, rtype, reaction):
        """ setreaction <type> <reaction>: Sets a reaction to a particular
            type, one of 'good', 'bad', 'meh', or 'unknown'. """
        if rtype not in ['good', 'bad', 'meh', 'unknown']:
            return badreactiontype
        return self.setreactiontype(reaction, rtype)
