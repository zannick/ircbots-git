# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""seendb -- A Redis-supported database of when a user was last seen in channel.

This Module allows users to check what a user was last seen doing in the channel,
such as joining or parting, or saying something. """

import re
import time
from irclib import nm_to_n
from redis import RedisError
from command import CharTriggerCommand, DISALLOW
from module import InstArg, ircbot_module, handles_event, public_command
from mredis import Redis

version = "SeenDB 0.5 by bswolf"

# Keys
seenkey = "seen:%s:%s" # STRING, arguments here are channel and nick

# Errors
unknownerror = "SeenDB: Unknown error in %s: %s"

nickregex = re.compile(r"^[@%+&]?(\S+)$")

# Event format strings
# note that 'nick' and 'quit' events don't supply a channel;
# the client has to determine for itself which channels it occurred in
formatstrings = {
        "action" : ("acted: '%s' at %s.", ("arg0", "evtime")),
        "join"   : ("joined %s at %s.", ("target", "evtime")),
        "kick"   : ("kicked %s at %s: %s.", ("arg0", "evtime", "arg1")),
        "mode"   : ("set mode %s at %s.", ("args", "evtime")),
        "nick"   : ("changed nick to %s at %s.", ("target", "evtime")),
        "part"   : ("left %s at %s: %s.", ("target", "evtime", "arg0")),
        "pubmsg" : ("said '%s' at %s.", ("arg0", "evtime")),
        "quit"   : ("quit IRC at %s: %s.", ("evtime", "arg0")),
        "topic"  : ('changed the topic to "%s" at %s.', ("arg0", "evtime"))
}
defaultformat = "triggered event %s > %s : %s at %s."

@ircbot_module
class SeenDB(Redis):
    """ A database that keeps track of a user's last activity in a channel. """
    def __init__(self, channels, host='localhost', port=6379, db=0,
                 password=None, socket_timeout=None, log=None, cmdchar='!'):
        """ Creates a Redis client for the specified server.
            channels - the bot's channels index
            log - optional function to log important errors and failures.
                  It should take a single string as an argument.
            cmdchar - The command character for the commands this class defines. """
        self.cmdchar = cmdchar # Done by Redis, but just to be sure
        super(SeenDB, self).__init__(host, port, db, password,
                                     socket_timeout, log, cmdchar)
        self.channels = channels
        # This is cleaner than putting n @handle tags on the function
        # Logging priority precedes channels index modification
        self.handlers += [(x, self.seen_event, -15) for x in formatstrings]

    # note that the use of 'namreply' here assumes that the bot does
    # not send NAMES requests any time other than upon joining a channel.
    @handles_event("namreply", -15)
    def on_namreply(self, c, e):
        args = e.arguments()
        chan = args[1]
        now = time.asctime()
        def stripchar(nick):
            m = nickregex.match(nick)
            if m:
                return m.group(1)
            return nick
        nicks = map(stripchar, args[2].strip().split(' '))
        self._update_all(chan, nicks,
                         "was in %s when I entered at %s." % (chan, now))

    @staticmethod
    def _format_event(e):
        msgtype = e.eventtype()
        now = time.asctime()
        if msgtype in formatstrings:
            fs = formatstrings[msgtype]
            src = e.source()
            if src:
                src = nm_to_n(src)
            target = e.target()
            args = e.arguments()
            d = {"src": src, "target": target, "arg0": "", "arg1": "",
                    "args": ' '.join(args), "evtime": now}
            if len(args) > 0:
                d["arg0"] = args[0]
            if len(args) > 1:
                d["arg1"] = args[1]
            return (fs[0] % tuple([d[k] for k in fs[1]]), now)
        else:
            return (defaultformat % (msgtype, e.target(), e.arguments(), now),
                    now)

    def seen_event(self, connection, event):
        """ Main event handler. """
        etype = event.eventtype()
        chat = event.target()
        src = event.source()
        if src:
            src = nm_to_n(src)
        seentext, now = self._format_event(event)

        # Special case comes first because these don't have associated channels
        if etype in ["nick", "quit"]:
            # multiple channels may get this event
            # TODO: Write self-contained channel/nick tracker
            for chan in self.channels:
                if self.channels[chan].has_user(src):
                    self._update(chan, src, seentext)
            return True
        elif etype == "part":
            # Check if it's the bot that's leaving
            if src == connection.get_nickname():
                self._update_all(chat, self.channels[chat].users(),
                                 "was in %s when I left at %s." % (chat, now))
        elif etype == "kick":
            # Also note the kickee
            kicker = src
            args = event.arguments()
            kickee = args[0]
            msg = args[1]
            self._update(chat, kickee, "was kicked from %s by %s at %s: %s." %\
                                        (chat, kicker, now, msg))
            # No return here so that the kicker's event can be logged below

        self._update(chat, src, seentext)
        return True

    def shutdown(self):
        """ Record all users in all channels as we leave. """
        now = time.asctime()
        for chan in self.channels:
            self._update_all(chan, self.channels[chan].users(),
                             "was in %s when I quit at %s." % (chan, now))
        super(SeenDB, self).shutdown()

    ## Main update functions ##

    def _update(self, channel, nick, msg):
        clower = channel.lower()
        nlower = nick.lower()
        try:
            self.redis.set(seenkey % (clower, nlower), msg)
        except RedisError as e:
            self.log(unknownerror % ("_update", e))

    def _update_all(self, channel, nicklist, msg):
        clower = channel.lower()
        try:
            p = self.redis.pipeline(transaction=True)
            for nick in nicklist:
                p.set(seenkey % (clower, nick.lower()), msg)
            rlist = p.execute()
            for r in rlist:
                if isinstance(r, Exception):
                    self.log(unknownerror % ("_update_all_atom", r))
        except RedisError as e:
            self.log(unknownerror % ("_update_all", e)) 

    ## Commands ##

    # Require this command in-channel as it makes use of the channel for lookup
    @public_command(CharTriggerCommand, InstArg("cmdchar"), "seen", [str],
                    DISALLOW, False)
    def seen(self, src, channel, nick):
        """ seen <nick>: Gives the specified user's last activity in the
            channel. """
        clower = channel.lower()
        nlower = nick.lower()
        try:
            msg = self.redis.get(seenkey % (clower, nlower))
            if msg:
                return "%s %s" % (nick, msg)
            return "I don't remember seeing %s in %s." % (nick, channel)
        except RedisError as e:
            self.log(unknownerror % ("lookup", e))

