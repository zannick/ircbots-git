# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

from irclib import nm_to_n
from module import Module, ircbot_module, handles_event

""" question -- A stateful way to handle user responses to bot questions.

This module adds a small amount of state to the bot by way of allowing it
to ask a user a question then react based on the response.

Be warned: using this functionality too often in public channels may make the
bot seem overly spammy. """

@ircbot_module
class Question(Module):
    """ This class allows the bot to ask a user a question or
        request information, and perform various actions based upon
        the user's answer. Questions stored here are by design not
        preserved between boots. """
    def __init__(self, channels, privmsg, notice, pubmsg):
        """ channels - A reference to the bot's channels index.
            privmsg - the bot's privmsg function
            notice - the bot's notice function
            pubmsg - the bot's pubmsg function
              These three should be function that cause the bot to
              send a message to a user (privmsg, notice) or a channel
              (pubmsg). They should take as arguments the connection,
              the user/channel being messaged, and the message itself.
              For pubmsg, there should be an optional 'target' argument
              after these, which is the user at which the message is
              directed. """
        super(Question, self).__init__()
        self.channels = channels
        self.privmsg = privmsg
        self.notice = notice
        self.pubmsg = pubmsg
        # self.db[channel][nick] is a function taking a string
        # defined by whatever registers the question. This function
        # should return (True, reply) if it answered the question,
        # (False, None) if not.
        self.db = {}
    
    def registerquestion(self, channel, target, callback):
        """ Register that a question has been asked of a user
            in a channel (or privmsg). When the target responds to the bot
            in that channel (or privmsg), the callback is called on
            the text of the reply. The callback should return a pair
            (True, reply) [reply may be None]
            if the reply was an answer to the question,
            (False, None) otherwise. """
        if channel not in self.db:
            self.db[channel] = {}
        self.db[channel][target] = callback

    # Logs at -15, commands at -10. At -12 we can log and not hit commands
    @handles_event("privmsg", -12)
    @handles_event("privnotice", -12)
    @handles_event("pubmsg", -12)
    @handles_event("pubnotice", -12)
    def _checkanswer(self, connection, event):
        if event.eventtype().endswith("notice"):
            reply = self.notice
        else:
            reply = self.privmsg
        src = event.source()
        channel = event.target()
        if src:
            src = nm_to_n(src)
        if channel.lower() == connection.get_nickname().lower():
            channel = src
        else:
            reply = lambda *args: self.pubmsg(*args, target=src)
        r, res = self.checkanswer(channel, src, event.arguments()[0])
        if r:
            reply(channel, res)
            return "NO MORE"

    def checkanswer(self, channel, nick, msg):
        """ Check to see if the message sent by nick to the bot
            in channel is an answer to a question asked by the bot.
            Returns (True, reply) if it was, (False, None) otherwise.
            If it was an answer, the question is deleted. """
        if channel not in self.db:
            return (False, None)
        if nick not in self.db[channel]:
            return (False, None)
        callback = self.db[channel][nick]
        if callback:
            # Organizing it this way lets callback set a follow-up question
            del self.db[channel][nick]
            result = callback(msg)
            if not(result[0]):
                self.db[channel][nick] = callback
            return result
        return (False, None)

    # Logging priority so that the channels index is not updated before this
    @handles_event("nick", -15)
    @handles_event("quit", -15)
    def _forcedelete(self, connection, event):
        src = nm_to_n(event.source())
        for chan in self.channels:
            if self.channels[chan].has_user(src):
                self.forcedelete(chan, src)
        self.forcedelete(src, src)

    # Always enforce question deletion in private messages.
    # These have to occur after the _checkanswer handlers.
    @handles_event("privmsg", -11)
    @handles_event("privnotice", -11)
    def _forcedelete_priv(self, connection, event):
        src = event.source()
        if src:
            src = nm_to_n(src).lower()
        self.forcedelete(src, src)

    def forcedelete(self, channel, nick):
        """ Clear any question associated with the given channel and nick. """
        if channel in self.db:
            if nick in self.db[channel]:
                del self.db[channel][nick]

