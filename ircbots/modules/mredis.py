# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""mredis -- Provides database access to a redis server.

This Module allows an ircbot to access and store data in a redis database,
rather than require a separate implementation of all the functionality it
provides, whether in python or in another language.

Redis is available at http://code.google.com/p/redis/. This Module also
requires the python interface to redis, available at
http://github.com/andymccurdy/redis-py. """

import redis
from module import Module, InstArg, ircbot_module, admin_command
from command import CharTriggerCommand

version = "Redis ircbot module v0.5 by bswolf"

@ircbot_module
class Redis(Module):
    """ This class acts as a client interface to the redis server. """
    def __init__(self, host="localhost", port=6379, db=0,
                 password=None, socket_timeout=None, log=None, cmdchar='!'):
        """ The client connection is set up with the given host, port, db,
            password, and socket timeout. 
            log - an optional function to log important errors and failures.
                  It should take a single string as an argument.
            cmdchar - The command character for the commands this class
                      defines. """
        self.cmdchar = cmdchar
        super(Redis, self).__init__()
        self.redis = redis.Redis(host, port, db, password, socket_timeout)
        self.log = log or (lambda s: None)

    def shutdown(self):
        try:
            self.redis.bgsave()
        except redis.ResponseError:
            # Already bgsaving?
            pass
        super(Redis, self).shutdown()

    ## Commands ##

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "getkey", [str])
    def getkey(self, src, chat, key):
        """ getkey <key>: Retrieves the value of a key from the redis database.
            This only works for single-item values, and not lists or sets. """
        try:
            return str(self.redis.get(key))
        except redis.ResponseError as e:
            return e
        except redis.RedisError as e:
            self.log("getkey error: " + e)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "sync", [])
    def sync(self, src, chat):
        """ sync: Save the database. """
        self.redis.bgsave()
        return "Syncing..."
