# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""logger -- Provides functions for logging observed events.

The Logger module will autorecord all chats the bot participates in,
as specified. It is possible to turn off this functionality if desired,
but as the Logger is integrated within the CommandBot (as the bot's replies
do not cause irc events), removing or preventing the bot from installing
this module may cause errors.
"""

import os
import time
from irclib import nm_to_n
from module import Module, InstArg, ircbot_module, admin_command
from command import CharTriggerCommand

version = "Logger 0.5 by bswolf"

# printing log mode
LOGNONE = -1
HIGHLIGHTS = 0
LOGALL = 1

# event format strings
# note that 'nick' and 'quit' events don't supply a channel;
# the client has to determine for itself which channels it occurred in
formatstrings = {
    'action'     : ("* %s %s", ("src", "arg0")),
    'ctcp'       : ("[CTCP %s from %s]: %s", ("arg0", "src", "arg1")),
    'ctcpreply'  : ("[CTCP %s reply from %s]: %s", ("arg0", "src", "arg1")),
    'join'       : ("-!- %s joined %s.", ("src", "target")),
    'kick'       : ("-!- %s kicked %s: %s", ("src", "arg0", "arg1")),
    'mode'       : ("-!- %s sets mode %s.", ("src", "args")),
    'nick'       : ("-!- %s changed nick to %s.", ("src", "target")),
    'part'       : ("-!- %s parted %s (%s).", ("src", "target", "arg0")),
    'privmsg'    : ("%s: %s", ("src", "arg0")),
    'privnotice' : ("(Notice) %s: %s", ("src", "arg0")),
    'pubmsg'     : ("%s: %s", ("src", "arg0")),
    'pubnotice'  : ("(Notice) %s: %s", ("src", "arg0")),
    'quit'       : ("-!- %s quit (%s).", ("src", "arg0")),
    'topic'      : ('-!- %s changed the topic to "%s".', ("src", "arg0"))
}
defaultformat = "Event %s: %s > %s : %s"

@ircbot_module
class Logger(Module):
    """ This class supplies functions for logging chatroom discussions
        and private conversations with the bot present, as well as
        important error logging. """
    def __init__(self, logdir, highlight, channels,
                 filenames="$chat/%Y-%m-%d.log", channellogmodes={},
                 cmdchar='!'):
        """ The first argument supplied should be a base directory
            to which the logs will be saved.

            The second argument is a function supplied by the bot that will
            take a message type (eg. 'act', 'ctcp', 'talk', 'part', etc.)
            and the string formatted for the log (minus the timestamp),
            and return whether or not the bot was highlighted.

            The third argument is the bot's channels index.

            The fourth, optional argument is a format
            for naming folders and files, to which strftime
            will be applied, in addition to the following replacements:
                $chat - the name of the chatroom or user the bot is chatting to
                $server - the name of the connection.

            The fifth argument is a per-channel dict of logmodes. These control
            what is recorded by the logger. If the mode for a channel is LOGALL,
            all messages are recorded. HIGHLIGHTS indicates that messages in
            channels that do not highlight the bot are not recorded. LOGNONE
            indicates that no messages should be recorded. The default logging
            mode is HIGHLIGHTS.
            Private messages are always recorded.

            The sixth argument is the command character for the logger's
            commands. """
        self.cmdchar = cmdchar
        super(Logger, self).__init__()
        self.logdir = logdir
        self.highlight = highlight
        self.channels = channels
        self.filename = filenames
        self.logmode = channellogmodes
        self.config += ["logmode"]
        # Cleaner to add handlers this way than to have 15 decorators
        self.handlers += [(x, self.logevent, -15) for x in formatstrings]

    def set_logging_mode(self, channel, logmode):
        """ Change the logging mode on a channel to one of LOGNONE, HIGHLIGHTS,
            or LOGALL. The logging mode determines when the bot records a
            message in its logs. LOGNONE disables all logging, HIGHLIGHTS
            allows logging only when the bot is addressed or referenced, and
            LOGALL enables all logging. """
        self.logmode[channel] = logmode

    def get_logging_mode(self, channel):
        """ Returns the logging mode on a channel, one of LOGNONE, HIGHLIGHTS,
            or LOGALL. If there is no logging mode set on a channel,
            returns None. """
        if channel in self.logmode:
            return self.logmode[channel]
        return None

    def _writelog(self, connection, chat, text, serverlog=False):
        logfile = self.logdir + "/" + self.filename
        if serverlog:
            logfile = logfile.replace("$chat", connection.server)
        else:
            logfile = logfile.replace("$chat", chat)
        logfile = logfile.replace("$server", connection.server)
        now = time.localtime()
        logfile = time.strftime(logfile, now)
        tstamp = time.strftime("[%H:%M:%S]", now)
        almostfullpath = logfile[:logfile.rindex('/')]
        if not os.path.exists(almostfullpath):
            os.makedirs(almostfullpath)
        if not os.path.exists(logfile):
            file = open(logfile, 'w')
            file.write("Logfile created by %s on %s.\n" %\
                    (connection.get_nickname(), time.asctime()))
        else:
            file = open(logfile, 'a')
        file.write("%s %s\n" % (tstamp, text))
        file.close()
        msg = "[%s]: %s %s" % (chat, tstamp, text)
        print msg

    @staticmethod
    def _format_event(e):
        msgtype = e.eventtype()
        if msgtype in formatstrings:
            fs = formatstrings[msgtype]
            src = e.source()
            if src:
                src = nm_to_n(src)
            target = e.target()
            args = e.arguments()
            d = { "src": src, "target": target, "arg0": "", "arg1": "",
                  "args": ' '.join(args) }
            if len(args) > 0:
                d["arg0"] = args[0]
            if len(args) > 1:
                d["arg1"] = args[1]
            return fs[0] % tuple([d[k] for k in fs[1]])
        else:
            return defaultformat % (msgtype, e.source(),
                                    e.target(), e.arguments())

    def logevent(self, connection, event):
        """ Signals to the logger that an event occurred. """
        # Set up important constants
        etype = event.eventtype()
        src = event.source()
        if src:
            src = nm_to_n(src)
        logtext = self._format_event(event)
        if etype in ["pubmsg", "pubnotice"]:
            highlit = self.highlight(etype, event.arguments()[0])
        elif etype in ["privmsg", "privnotice"]:
            highlit = True
        else:
            highlit = self.highlight(etype, logtext)

        # Special case comes first because these don't have associated channels
        if etype in ["nick", "quit"]:
            logged = False
            # multiple channels may get this event
            # TODO: Write self-contained channel/nick tracker
            for chan in self.channels:
                if self.channels[chan].has_user(src):
                    if chan in self.logmode:
                        lm = self.logmode[chan]
                        if lm is LOGALL or lm is HIGHLIGHTS and highlit:
                            self._writelog(connection, chan, logtext)
                            logged = True
                    else:
                        self.logmode[chan] = HIGHLIGHTS
                        if highlit:
                            self._writelog(connection, chan, logtext)
                            logged = True
            return logged

        # ctcp actions occur in ctcp and action, log them only in action
        if etype == "ctcp" and event.arguments()[0].lower() == "action":
            return False

        serverlog = False

        # Should we log this? return False if we don't
        chat = event.target().lower()
        if chat in self.logmode:
            if self.logmode[chat] is LOGNONE:
                return False
            elif self.logmode[chat] is HIGHLIGHTS:
                if not highlit:
                    return False
        # Always log privmsgs (chat = botname are privmsgs)
        elif chat != connection.get_nickname().lower():
            # A channel not in the list
            self.logmode[chat] = HIGHLIGHTS
            if not highlit:
                return False
        else:
            # privmsgs
            chat = src
            if src.lower() == 'nickserv':
                serverlog = True
            elif event.arguments()[0].lower().startswith('[global notice]'):
                serverlog = True

        if chat == '*' or '.' in src:
            serverlog = True

        # Every case that lands here should be logged.
        self._writelog(connection, chat, logtext, serverlog)
        return True

    def logself(self, connection, etype, target="", msg="",
                ctcptype="", kicked=""):
        """ Logs an event whose source is the bot. These events are not
            received through the event system, and so must be kept track of
            separately. """
        d = { "src": connection.get_nickname(), "target": target,
              "arg0": msg, "arg1": msg, "args": msg }
        if etype in ["ctcp", "ctcpreply"]:
            d["arg0"] = ctcptype
        elif etype == "kick":
            d["arg0"] = kicked
        
        if etype in formatstrings:
            fs = formatstrings[etype]
            logtext = fs[0] % tuple([d[k] for k in fs[1]])
        else:
            fs = defaultformat
            logtext = fs % (etype, d["src"], target, msg)
        
        if etype in ["nick", "quit"]:
            logged = False
            for chan in self.channels:
                if chan in self.logmode:
                    if self.logmode[chan] is not LOGNONE:
                        self._writelog(connection, chan, logtext)
                        logged = True
                else:
                    self.logmode[chan] = HIGHLIGHTS
                    self._writelog(connection, chan, logtext)
                    logged = True
            return logged

        chat = target
        if chat in self.logmode:
            if self.logmode[chat] is not LOGNONE:
                self._writelog(connection, chat, logtext)
                return True
        else:
            self.logmode[chat] = HIGHLIGHTS
            self._writelog(connection, chat, logtext)
            return True
        return False

    ## Commands ##
    
    @admin_command(CharTriggerCommand, InstArg("cmdchar"),
                   "setlogmode", [str, str])
    def setlogmode(self, src, chat, channel, mode):
        """ setlogmode <channel> <mode>: Sets the logging mode for the
            specified channel to <mode>. There are three modes: 'on', 'off',
            and 'highlights'. """
        lm = mode.lower()
        if lm in ["off", "none"]:
            self.set_logging_mode(channel, LOGNONE)
            return "Logging disabled for %s." % channel
        elif lm in ["on", "all"]:
            self.set_logging_mode(channel, LOGALL)
            return "Logging enabled for %s." % channel
        elif lm in ["hilight", "highlight", "hilights", "highlights"]:
            self.set_logging_mode(channel, HIGHLIGHTS)
            return "Logging set to highlights only for %s." % channel
        return "Log mode '%s' not understood."

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "getlogmode", [str])
    def getlogmode(self, src, chat, channel):
        """ getlogmode <channel>: Returns the current log mode for the
            specified channel, if one exists. """
        if channel in self.logmode:
            lm = self.logmode[channel]
            fm = { LOGNONE: "none", LOGALL: "all" }
            m = lm in fm and fm[lm] or "highlights"
            return "Logging in %s is currently set to '%s'." % (channel, m)
        else:
            return "I have no logging set for %s." % channel

