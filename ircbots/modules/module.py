# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""module -- A small class that can be easily added to an ircbot.

A Module is a class that modifies or enhances some of the bot's behavior
in some area, through a set of commands and event handlers. The event handlers
trigger on irc events (such as privmsg, pubmsg, join, ping, etc.), while the
commands deal with special cases where the bot is expected to perform some
action and reply.

A Module should only speak for the bot through its commands' return values.
This standardization allows modules (such as logger) to properly handle the bot
speaking, without requiring every module to reference it.

A Module, like the bot itself, may define some values that it wants to persist
across bot reboots. The names of these values should be put into the Module's
config list.

This module defines five decorators for to ease defining event handlers and
command handlers. The first is @ircbot_module, which must be used to decorate
any class whose methods use the other four, in order to have that class
correctly recognize which handlers and commands it has.

The other four decorators, @handles_event, @admin_command,
@private_command, and @public_command, are all function
decorators to be used to designate which internal functions are handlers for
given commands or events.

The decorators are used with the assumption that the functions are instance
methods whose first argument should be the object itself. For class methods
and static methods, the functions can still be used as handlers, but they
have to be defined and inserted into the proper list manually in __init__.

Having Modules extend other Modules is dangerous, because if the parent Module
defines any commands and handlers, traditionally, they would both be added if
the bot uses two separate child Modules. For this reason, it is better to pass
one of the Modules as an argument to the other in instantiation.

Note that if you do use Modules that extend other Modules, the commands and
handlers defined in the parent Module(s) are not added to the bot's lists of
handlers if you use the decorators as described.
This can be useful in some cases if you want the functionality of
the other Module without needing its commands or handlers. """

class Module(object):
    """ The base class for modules to be installed into an ircbot. """
    def __init__(self):
        """ Instantiates the requisite set of lists of event handlers,
            config variables, and command arguments, to be retrieved by the
            bot when the module is installed.

            A handler should be a pair or triple of elements, either
            (event, callback) or (event, callback, priority), where event is
            a string and callback is a function that takes a connection
            object and an event object. Note that not listing
            a priority is the same as passing a priority of 0.
            
            A command should be a Command object and not a set of arguments.

            A config variable is an attribute of the module object that is to be
            persistent across bot reboots. These will be added to the bot's
            config file. """
        self.handlers = _get_event_handlers(self)
        # Lists of commands
        (self.admin_commands, self.private_commands, 
         # Lists of triples: (command, pm_option, targeted)
         self.public_commands) = _get_command_handlers(self)
        self.config = []

    def get_handlers(self):
        return self.handlers

    def get_commands(self):
        """ Returns a triple of command lists.
            The order is always admin, private, public. """
        return (self.admin_commands, self.private_commands, self.public_commands)

    def shutdown(self):
        """ This function will be called when the bot is shutting down.
            A module should implement this function when it needs to perform
            any cleanup before the bot terminates, other than saving config
            variables, which is done by the bot. """
        pass

class ModuleError(Exception):
    pass

class InstArg(object):
    """ Defines an instance argument to be used with the function decorators,
        such as for the case where a command or event handler needs to use an
        argument given in the Module's constructor.
        
        The specified variables should be set as 'self.name' before the
        Module's constructor invoked super.__init__. """
    def __init__(self, name):
        self.name = name

    def get_from(self, obj):
        if hasattr(obj, self.name):
            return getattr(obj, self.name)
        raise ModuleError("Object of class %s has no attribute %s." % \
                          (extract_module_name(obj.__class__), self.name))

def extract_module_name(cls):
    return "%s.%s" % (cls.__module__, cls.__name__)

from functools import partial
from command import ALLOW

# function decorators for handlers and commands

__tmp_event_handlers = []
__tmp_commands = { "admin" : [], "private" : [], "public" : [] }

__event_handlers = {}
__command_handlers = {}

def _get_event_handlers(obj):
    cls = obj.__class__
    key = "%s.%s" % (cls.__module__, cls.__name__)
    if key in __event_handlers:
        return [(event, getattr(obj, f.__name__), priority)
                for (event, f, priority) in __event_handlers[key]]
    return []

def _get_command_handlers(obj):
    cls = obj.__class__
    key = "%s.%s" % (cls.__module__, cls.__name__)
    if key in __command_handlers:
        def make_arglist(args, f):
            arglist = [isinstance(a, InstArg) and a.get_from(obj) or a
                       for a in args]
            arglist.append(getattr(obj, f.__name__))
            return arglist
        return ([cls(*make_arglist(args, f))
                 for (cls, args, _, f) in __command_handlers[key]["admin"]],
                [cls(*make_arglist(args, f))
                 for (cls, args, _, f) in __command_handlers[key]["private"]],
                [(cls(*make_arglist(args, f)), p[0], p[1])
                 for (cls, args, p, f) in __command_handlers[key]["public"]])
    return ([], [], [])

def ircbot_module(cls):
    global __tmp_event_handlers, __tmp_commands
    key = "%s.%s" % (cls.__module__, cls.__name__)
    __event_handlers[key] = __tmp_event_handlers
    __command_handlers[key] = __tmp_commands
    __tmp_event_handlers = []
    __tmp_commands = { "admin" : [], "public" : [], "private" : [] }
    return cls

def handles_event(event, priority=0):
    def makecallback(f):
        global __tmp_event_handlers
        __tmp_event_handlers += [(event, f, priority)]
        return f
    return makecallback

def admin_command(cls, *args):
    def makecommand(f):
        __tmp_commands["admin"].append((cls, args, (), f))
        return f
    return makecommand

def private_command(cls, *args):
    def makecommand(f):
        __tmp_commands["private"].append((cls, args, (), f))
        return f
    return makecommand

def public_command(cls, *args, **kwargs):
    # TODO: Convert to named arguments after *args, requires python 3
    pm_option = "pm_option" in kwargs and kwargs["pm_option"] or ALLOW
    targeted = "targeted" in kwargs and kwargs["targeted"] # default False
    def makecommand(f):
        __tmp_commands["public"].append((cls, args, (pm_option, targeted), f))
        return f
    return makecommand

