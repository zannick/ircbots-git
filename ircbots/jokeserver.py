# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""A commandbot that keeps a library of jokes to tell.
"""

import re
from threading import Timer
from cslounge import csloungify
from responsebot import ResponseDict, ResponseBot
from modules.jokedb import JokeDB
from modules.mredis import Redis
from modules.question import Question
from modules.zmap import Zmap

from pswd import rpswd, jpswd

hostname = "irc.freenode.net"
port = 6667
chanlist = ["#cslounge"]
name = "jokeserver"
admins = ['Zannick']
version = "jokeserver v0.7 by bswolf"

kernelregex = re.compile(r"([Gg]ood )?(([Mm]or|[Ee]ve)ning|[Nn]ight|"
                         r"[Dd]ay|[Aa]fternoon), programs[.!?]?")

@csloungify
class jokeserver(ResponseBot):
    def __init__(self):
        responsedict = ResponseDict(db=1, password=rpswd,
                                    log=self.logimportant)
        super(jokeserver, self).__init__(responsedict, hostname, port,
                                         chanlist, name, admins,
                                         password=jpswd, version=version,
                                         username='puns')
        redis = Redis(db=1, password=rpswd, log=self.logimportant)
        question = Question(self.channels, self.privmsg,
                            self.privnotice, self.pubmsg)
        zmap = Zmap('/home/bswolf/zmap')
        jokedb = JokeDB(question, self.channels, self.privmsg, self.pubmsg,
                        db=1, password=rpswd, log=self.logimportant)
        modules = [redis, question, zmap, jokedb]
        for m in modules:
            self.install_module(m)

    def handle_untargeted(self, c, e, src, msg):
        s = super(jokeserver, self).handle_untargeted(c, e, src, msg)
        if s:
            return s
        # inside joke
        if not hasattr(self, 'nwf'):
            if re.match(r"nwf_?(\|pocket)?$", src):
                m = kernelregex.match(msg.lstrip())
                if m:
                    self.nwf = True
                    def t():
                        del self.nwf
                    Timer(36000, t).start() # 10 hours
                    return m.group(0).replace("programs", "kernel")

if __name__ == "__main__":
    bot = jokeserver()
    bot.start()
