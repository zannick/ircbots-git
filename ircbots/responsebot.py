# Copyright (C) 2009--2010 Benjamin S Wolf
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA
#
# Benjamin S Wolf <jokeserver@gmail.com>

"""An irc bot that can respond pseudo-conversationally to users talking
to it.

This file contains a module, ResponseDict, and a bot class extending
CommandBot that includes this module. The class extension is necessary
as the integration of ResponseDict requires being referenced from the
handle_targeted stub method CommandBot defines.

The ResponseDict relies on the Redis module defined in modules/, as it
is a subclass of it.

The ResponseDict presented here is extremely simple, but it is not the focus
of this project, hence it will not likely be improved.
"""

import random
from commandbot import CommandBot
from modules.command import CharTriggerCommand
from modules.module import Module, InstArg, ircbot_module, admin_command
from modules.mredis import Redis

version = "ResponseBot 0.1 by bswolf"

aliaskey = "alias:%s"
responsekey = "response:%s"
noresponsekey = "noresponse:%s"

@ircbot_module
class ResponseDict(Redis):
    """ Maintains a Redis-based mapping of statements to responses,
        and aliases. """
    def __init__(self, host="localhost", port=6379, db=0,
                 password=None, socket_timeout=None, log=None, cmdchar='!'):
        """ The client connection is set up with the given host, port, db,
            password, and socket timeout.
            log - an optional function to log important errors and failures.
                  It should take a single string as an argument.
            cmdchar - the command character for the commands this class
                      defines. """
        self.cmdchar = cmdchar
        super(ResponseDict, self).__init__(host, port, db, password,
                                           socket_timeout, log, cmdchar)

    def _lookup(self, trigger):
        while not(self.redis.exists(responsekey % trigger)):
            if self.redis.exists(aliaskey % trigger):
                trigger = self.redis.get(aliaskey % trigger)
            else:
                break
        if self.redis.exists(responsekey % trigger):
            responses = self.redis.smembers(responsekey % trigger)
            r = random.choice(list(responses))
            return r
        return None

    def handle_response(self, c, e, src, msg):
        # A response key exists in the form
        # response:text
        # And is a set of possible answers, of which one is chosen (randomly)
        # An alias key is in the form alias:text
        # and is a string indicating another trigger to look up
        mlower = msg.lower().strip()
        target = e.target()
        is_privmsg = target == c.get_nickname()
        if mlower.startswith('[global notice]') or src == '*':
            return None
        trigger = mlower.replace(' ', '_')
        if self.redis.get(noresponsekey % trigger):
            return None
        r = self._lookup(trigger)
        if r:
            return r.replace("\src", src)

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "addresponse",
                   [str])
    def addresponse(self, src, chat, s):
        """ addresponse <string>: Add a response to the response dict.
            The trigger should precede one of "<reply>", "<alias>", or
            "<noreply>". <reply> should be followed by a response,
            <alias> should be followed by another trigger,
            and <noreply> does not require any text after it. """
        if s.find("<reply>") >= 0:
            qr = s.split("<reply>", 1)
            q = qr[0].lower().strip().replace(' ', '_')
            r = qr[1].strip()
            self.redis.sadd(responsekey % q, r)
        elif s.find("<alias>") >= 0:
            qr = s.split("<alias>", 1)
            q = qr[0].lower().strip().replace(' ', '_')
            r = qr[1].lower().strip().replace(' ', '_')
            self.redis.set(aliaskey % q, r)
        elif s.find("<noreply>") >= 0:
            q = s[:s.index("<noreply")].lower().strip().replace(' ', '_')
            self.redis.set(noresponsekey % q, 1)
        else:
            return "Use one of <reply>, <alias>, or <noreply>."
        return "Okay, I'll remember that for later."

    @admin_command(CharTriggerCommand, InstArg("cmdchar"), "remresponse",
                   [str])
    def remresponse(self, src, chat, s):
        """ remresponse <string>: Remove a response. The trigger should
            precede "<reply>" and the response should follow. """
        if s.find("<reply>") >= 0:
            qr = s.split("<reply>", 1)
            q = qr[0].lower().strip().replace(' ', '_')
            r = qr[1].strip()
            if self.redis.type(responsekey % q) == "set":
                if self.redis.srem(responsekey % q, r):
                    return "Removed."
                return "I didn't have that response."
            return "I don't have responses to that."
        return "Please use <reply> to separate the trigger and response."

class ResponseBot(CommandBot):
    """ Implements a bot that can chat on IRC.
        In addition to the capabilities provided by CommandBot, this bot
        can respond to statements and questions made to it (in a very
        limited manner). """
    def __init__(self, responsedict, hostname, port, chanlist, name, admins,
            password=None, apass=None, cmdchar='!', pm_only_cmds=True,
            modules=[], version=version, username=None):
        """ responsedict - the ResponseDict module object for this bot
            hostname - IRC server hostname to connect to
            port - port on the server
            chanlist - list of channels to join upon connecting
            name - nick of the bot
            admins - nonempty list of nicknames of bot admins
            password - the password the bot uses to identify itself to NickServ
            apass - admin password that admins must use to identify
                    themselves to the bot. if None, admins need only to be
                    identified to NickServ when talking to the bot
            cmdchar - the command character for help, cmdinfo, and acmdinfo
            pm_only_cmds - True if help, cmdinfo, version are PM-only commands
            modules - a list of Module objects to be installed.
            version - a version string about the bot.
            username - the username field for irc (as seen via whois). """
        self.responsedict = responsedict
        super(ResponseBot, self).__init__(hostname, port, chanlist, name,
                admins, password, apass, cmdchar, pm_only_cmds,
                [responsedict] + modules, version, username)

    def handle_targeted(self, c, e, src, msg):
        s = super(ResponseBot, self).handle_targeted(c, e, src, msg)
        if s:
            return s
        return self.responsedict.handle_response(c, e, src, msg)

